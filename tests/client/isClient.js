import { Meteor } from 'meteor/meteor'
import { assert } from 'chai'

describe('Client', function() {
  it('is not server', function() {
    assert.equal(Meteor.isServer, false)
  })
})

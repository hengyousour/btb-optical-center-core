import { Meteor } from 'meteor/meteor'
import { assert } from 'chai'

describe('Server', function() {
  it('is not client', function() {
    assert.equal(Meteor.isClient, false)
  })
})

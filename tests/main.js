import { Meteor } from 'meteor/meteor'

/**
 * Server
 */
if (Meteor.isServer) {
  // require('/tests/server')
  // require('/imports/modules/blog/tests/server')
  require('/imports/modules/pos/tests/server')
}

// Client
if (Meteor.isClient) {
  // require('/tests/client')
  // require('/imports/modules/blog/tests/client')
  require('/imports/modules/pos/tests/client')
}

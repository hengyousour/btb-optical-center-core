# Changelog

## V1.0.0 (2018-02-15)

#### New features

-

#### Bug fixes

-

#### Break changes

- Refactor `accounting` nav bar

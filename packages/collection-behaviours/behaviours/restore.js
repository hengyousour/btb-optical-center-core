import { Meteor } from 'meteor/meteor'
import SimpleSchema from 'simpl-schema'

// Restore
CollectionExtensions.addPrototype('restore', function(selector = {}) {
  let self = this
  const schema = new SimpleSchema({
    removed: {
      type: Boolean,
      optional: true,
    },
    restoredAt: {
      type: Date,
      optional: true,
    },
    restoreBy: {
      type: String,
      optional: true,
    },
  })
  // self.attachSchema(schema, {selector: {type: 'restore'}});
  self.attachSchema(schema)

  return self.direct.update(
    selector,
    {
      $set: {
        removed: false,
        restoredAt: new Date(),
        restoreBy: Meteor.userId(),
      },
    },
    {
      multi: true,
      // selector: {type: 'restore'}
    },
    (err, res) => {
      if (err) {
        throw new Meteor.Error('restore', 'Restore Error', err)
      }
      // console.log('restore')
    }
  )
})

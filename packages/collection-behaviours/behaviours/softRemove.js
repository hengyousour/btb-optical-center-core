import { Meteor } from 'meteor/meteor'
import SimpleSchema from 'simpl-schema'

// Soft Remove
CollectionExtensions.addPrototype('softRemove', function(selector = {}) {
  let self = this
  const schema = new SimpleSchema({
    removed: {
      type: Boolean,
      optional: true,
    },
    removedAt: {
      type: Date,
      optional: true,
    },
    removedBy: {
      type: String,
      optional: true,
    },
  })
  // self.attachSchema(schema, {selector: {type: 'softRemove'}});
  self.attachSchema(schema)

  return self.direct.update(
    selector,
    {
      $set: {
        removed: true,
        removedAt: new Date(),
        removedBy: Meteor.userId(),
      },
    },
    {
      multi: true,
      // selector: {type: 'softRemove'}
    },
    (err, res) => {
      if (err) {
        throw new Meteor.Error('soft-remove', 'Soft-Remove Error', err)
      }
      // console.log('soft-remove')
    }
  )
})

CollectionExtensions.addPrototype('timestamp', function() {
  let self = this

  self.before.insert((userId, doc) => {
    doc.createdAt = new Date()
    doc.createdBy = userId
    // console.log('create-timestamp')
  })
  self.before.update((userId, doc, fieldNames, modifier, options) => {
    modifier.$set = modifier.$set || {}
    modifier.$set.updatedAt = new Date()
    modifier.$set.updatedBy = userId
    // console.log('update-timestamp')
  })
})

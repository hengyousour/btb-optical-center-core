Package.describe({
  name: 'theara:collection-behaviours',
  version: '0.0.5',
  // Brief, one-line summary of the package.
  summary: 'Extends Mongo.Collection with behaviour patterns',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md',
})

Npm.depends({
  'simpl-schema': '1.5.0',
})

let both = ['client', 'server']

Package.onUse(function(api) {
  api.versionsFrom('1.7.0.3')
  api.use(
    [
      'ecmascript',
      'lai:collection-extensions@0.2.1_1',
      'matb33:collection-hooks@0.8.4',
    ],
    both
  )

  api.addFiles(
    [
      'behaviours/timestamp.js',
      'behaviours/softRemove.js',
      'behaviours/restore.js',
    ],
    both
  )

  api.mainModule('collection-behaviours.js')
})

Package.onTest(function(api) {
  api.use('ecmascript')
  api.use('tinytest')
  api.use('theara:collection-behaviours')
  api.mainModule('collection-behaviours-tests.js')
})

// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by collection-behaviours.js.
import { name as packageName } from "meteor/theara:collection-behaviours";

// Write your tests here!
// Here is an example.
Tinytest.add('collection-behaviours - example', function (test) {
  test.equal(packageName, "collection-behaviours");
});

# Mongo Collection Behaviours

### Get started

```
meteor add theara:collection-behaviours
```

### Usage

```js
var Test = new Mongo.Collection('test')

Test.timestamp()
Test.softRemove({})
Test.restore({})
```

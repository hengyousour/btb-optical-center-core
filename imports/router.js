import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

//------ Module -----
import app from './client/routes'
import demo from '/imports/modules/demo/client/routes'

const router = new VueRouter({
  mode: 'history',
  // routes: app,
  routes: app.concat(demo),
})

export default router

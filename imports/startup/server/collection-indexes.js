import { Meteor } from 'meteor/meteor'

import Employees from '../../api/employees/employees'

Meteor.startup(() => {
  Employees._ensureIndex({ code: 1, branchId: 1 }, { unique: true })
})

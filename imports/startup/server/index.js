/*** Core ***/
import './accounts'
import './collection-indexes'
import './register-api'
import './register-utils'
import './fixtures'

import { Meteor } from 'meteor/meteor'
import { Roles } from 'meteor/alanning:roles'
import { Accounts } from 'meteor/accounts-base'
import moment from 'moment'

import Company from '../../api/company/company'
import Branches from '../../api/branches/branches'
import Currency from '../../api/currency/currency'

import { insertBranch } from '../../api/branches/methods'

Meteor.startup(function() {
  /**
   * Development
   */
  // if (Meteor.isDevelopment) {
  // }

  /**
   * Production
   */
  // Company
  if (Company.find().count() === 0) {
    const data = {
      _id: '1',
      name: 'មជ្ឈមណ្ឌលបណ្តុះបណ្តាល រ៉ាប៊ីត',
      address: 'ខេត្តបាត់ដំបង',
      telephone: '070/085 550 880',
      email: 'yuom.theara@gmail.com',
      website: 'http://rabbit-tech.com',
      industry: 'IT',
      setting: {
        // Start date
        fiscalDate: moment()
          .startOf('year')
          .toDate(),
        baseCurrency: 'USD',
        decimalNumber: 2,
        accountingIntegration: true,
        dateFormat: 'DD/MM/YYYY H:mm:ss',
        lang: 'EN',
      },
    }

    Company.insert(data)
  }

  // Branch
  if (Branches.find().count() === 0) {
    const data = [
      {
        _id: '01',
        name: 'Head Office',
        shortName: 'HO',
        address: 'Battambang Province',
        telephone: '070/085 550 880',
        email: 'yuom.theara@gmail.com',
      },
    ]
    data.forEach(doc => {
      insertBranch.run(doc)
    })
  }

  // Currency
  if (Currency.find().count() === 0) {
    const data = [
      {
        _id: 'KHR',
        name: 'Khmer Riel',
        symbol: '៛',
      },
      {
        _id: 'USD',
        name: 'US Dollar',
        symbol: '$',
      },
      {
        _id: 'THB',
        name: 'Thai Baht',
        symbol: 'B',
      },
    ]

    data.forEach(doc => {
      Currency.insert(doc)
    })
  }

  // Roles
  // if (Roles.getAllRoles().count() === 0) {
  const data = [
    'super',
    'admin-setting',
    // Accounting
    'account-setting',
    'journal-insert',
    'journal-update',
    'journal-remove',
    // Report
    'journal-report',
    'ledger-report',
    'trial-balance-report',
    'profit-loss-report',
    'balance-sheet-report',
    'transaction-list-report',
  ]
  const allRoles = Roles.getAllRoles().fetch()
  data.forEach(role => {
    // Check role exist
    if (!allRoles.find(o => o.name === role)) {
      Roles.createRole(role)
    }
  })
  // }

  // User
  if (Meteor.users.find().count() === 0) {
    const data = [
      {
        _id: '1',
        username: 'super',
        email: 'super@rabbit.com',
        password: Meteor.isDevelopment
          ? '123456'
          : `superpwd@${moment().format('YYYY')}`,
        profile: {
          fullName: 'Rabbit Super',
          allowedBranches: ['01'],
          status: 'Active',
        },
        roles: 'super',
      },
      // {
      //   _id: '2',
      //   username: 'admin',
      //   email: 'admin@rabbit.com',
      //   password: '123456',
      //   profile: {
      //     fullName: 'Admin',
      //     allowedBranches: ['01'],
      //     status: 'Active',
      //   },
      //   roles: 'admin',
      // },
    ]

    data.forEach(({ username, email, password, profile, roles }) => {
      const userExists = Accounts.findUserByUsername(username)

      if (!userExists) {
        const userId = Accounts.createUser({
          username,
          email,
          password,
          profile,
        })
        Roles.addUsersToRoles(userId, roles)
      }
    })
  }
})

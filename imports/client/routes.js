// Main
import NotFound from './pages/NotFound.vue'
import Login from './pages/Login.vue'
import Confirm from './pages/Confirm.vue'
import Dashboard from './pages/Dashboard.vue'

// Admin Setting
import AdminSetting from './pages/AdminSetting.vue'

// Accounting
import AccountCenter from './pages/AccountCenter.vue'
import AccountSetting from './pages/AccountSetting.vue'
import ChartAccountForm from './pages/ChartAccountForm.vue'
import JournalForm from './pages/JournalForm.vue'

// Report
import ReportCenter from './reports/Index.vue'
import AccountListReport from './reports/AccountList.vue'
import JournalReport from './reports/Journal.vue'
import GeneralLedgerReport from './reports/GeneralLedger.vue'
import TrialBalanceReport from './reports/TrialBalance.vue'
import ProfitAndLossReport from './reports/ProfitAndLoss.vue'
import BalanceSheetReport from './reports/BalanceSheet.vue'

const routes = [
  // Not Found
  {
    path: '*',
    name: 'NotFound',
    component: NotFound,
    meta: {
      title: 'Not Found',
      noCache: true,
    },
  },
  // Login
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: {
      layout: 'login',
      title: 'Login',
      noAuth: true,
      noCache: true,
    },
  },
  // Confirm
  {
    path: '/confirm',
    name: 'Confirm',
    component: Confirm,
    meta: {
      layout: 'login',
      title: 'Confirm',
      noCache: true,
    },
  },
  // Dashboard
  {
    path: '/',
    name: 'Dashboard',
    component: Dashboard,
    meta: {
      title: 'Dashboard',
      icon: 'fas fa-tachometer-alt',
      noCache: true,
    },
  },
  /**
   * Admin Setting
   */
  {
    path: '/admin-setting/:active?',
    name: 'AdminSetting',
    component: AdminSetting,
    meta: {
      title: 'Admin Setting',
      breadcrumb: {
        parent: 'Dashboard',
      },
    },
  },
  /**
   * Accounting Center
   */
  {
    path: '/account',
    name: 'AccountCenter',
    component: AccountCenter,
    meta: {
      title: 'Accounting Center',
      noCache: true,
      breadcrumb: {
        parent: 'Dashboard',
      },
    },
  },
  // Account Setting
  {
    path: '/account-setting/:active?',
    name: 'AccountSetting',
    component: AccountSetting,
    meta: {
      title: 'Account Setting',
      linkActive: 'AccountCenter',
      breadcrumb: {
        parent: 'Dashboard',
      },
    },
  },
  // Chart of Account
  {
    path: '/chart-account/:showId?',
    name: 'ChartAccountForm',
    component: ChartAccountForm,
    props: true,
    meta: {
      title: 'Chart Account',
      linkActive: 'AccountCenter',
      breadcrumb: {
        parent: 'Dashboard',
      },
    },
  },
  // Journal
  {
    path: '/journal/:_id?',
    name: 'JournalForm',
    component: JournalForm,
    meta: {
      title: 'Journal Entry',
      linkActive: 'AccountCenter',
      breadcrumb: {
        parent: 'AccountCenter',
      },
    },
  },
  /**
   * Report Center
   */
  {
    path: '/report',
    name: 'ReportCenter',
    component: ReportCenter,
    meta: {
      title: 'Reports',
      breadcrumb: {
        parent: 'Dashboard',
      },
    },
  },
  // Account List
  {
    path: '/account-list-rpt',
    name: 'AccountListReport',
    component: AccountListReport,
    meta: {
      title: 'Account List Report',
      linkActive: 'Report',
      breadcrumb: {
        parent: 'Dashboard',
      },
    },
  },
  // Journal
  {
    path: '/journal-rpt',
    name: 'JournalReport',
    component: JournalReport,
    meta: {
      title: 'Journal Report',
      linkActive: 'Report',
      breadcrumb: {
        parent: 'Dashboard',
      },
    },
  },
  // General Ledger
  {
    path: '/general-ledger-rpt',
    name: 'GeneralLedgerReport',
    component: GeneralLedgerReport,
    meta: {
      title: 'General Ledger Report',
      linkActive: 'Report',
      breadcrumb: {
        parent: 'Dashboard',
      },
    },
  },
  // Trail Balance
  {
    path: '/trial-balance-rpt',
    name: 'TrialBalanceReport',
    component: TrialBalanceReport,
    meta: {
      title: 'Trial Balance Report',
      linkActive: 'Report',
      breadcrumb: {
        parent: 'Dashboard',
      },
    },
  },
  // Profit and Loss
  {
    path: '/profit-loss-rpt',
    name: 'ProfitLossReport',
    component: ProfitAndLossReport,
    meta: {
      title: 'Profit And Loss Report',
      linkActive: 'Report',
      breadcrumb: {
        parent: 'Dashboard',
      },
    },
  },
  // Balance Sheet
  {
    path: '/balance-sheet-rpt',
    name: 'BalanceSheetReport',
    component: BalanceSheetReport,
    meta: {
      title: 'Balance Sheet Report',
      linkActive: 'Report',
      breadcrumb: {
        parent: 'Dashboard',
      },
    },
  },
]

export default routes

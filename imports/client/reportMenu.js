const reportMenu = [
  {
    title: 'Accounting',
    icon: 'fas fa-dollar-sign',
    activeGroups: ['General'],
    data: [
      // General
      {
        groupTitle: 'General',
        icon: 'fas fa-print',
        items: [
          {
            title: 'Journal',
            route: { name: 'JournalReport' },
            memo: ``,
          },
          {
            title: 'General Ledger',
            route: { name: 'GeneralLedgerReport' },
            memo: ``,
          },
          {
            title: 'Trial Balance',
            route: { name: 'TrialBalanceReport' },
            memo: ``,
          },
          {
            title: 'Profit & Loss',
            route: { name: 'ProfitLossReport' },
            memo: ``,
          },
          {
            title: 'Balance Sheet',
            route: { name: 'BalanceSheetReport' },
            memo: ``,
          },
          {
            title: 'Account List',
            route: { name: 'AccountListReport' },
            memo: ``,
          },
        ],
      },
      // Summary
      // {
      //   groupTitle: 'Summary',
      //   icon: 'fas fa-print',
      //   items: [
      //     {
      //       title: 'Account List',
      //       route: { name: 'AccountListReport' },
      //       memo: ``,
      //     },
      //   ],
      // },
    ],
  },
]

export default reportMenu

const sidebarMenu = [
  // { header: 'Header' },
  // { divider: true },
  {
    title: 'Accounting',
    icon: 'fas fa-dollar-sign',
    route: { name: 'AccountCenter' },
    children: [
      {
        title: 'Make Journal',
        route: { name: 'JournalForm' },
      },
      {
        title: 'Chart of Account',
        route: { name: 'ChartAccountForm' },
      },
      { divider: true },
      {
        title: 'Setting',
        route: { name: 'AccountSetting' },
      },
    ],
  },
  {
    title: 'Reports',
    icon: 'fas fa-file-invoice',
    route: { name: 'ReportCenter' },
  },
  {
    title: 'Admin Setting',
    icon: 'fas fa-cogs',
    route: { name: 'AdminSetting' },
  },
]

export default sidebarMenu

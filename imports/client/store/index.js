import { Meteor } from 'meteor/meteor'
import { Tracker } from 'meteor/tracker'
import { Session } from 'meteor/session'
import { Roles } from 'meteor/alanning:roles'
import _ from 'lodash'
import moment from 'moment'

// Router
import router from '/imports/router'

// Modules
import tagView from './tagView'

export default {
  namespaced: true,
  state: {
    lang: 'en',
    company: Session.get('company'),
    currentUser: Meteor.user() || Session.get('currentUser'),
    allowedBranches: Session.get('allowedBranches') || [],
    currentBranch: Session.get('currentBranch'),
  },
  getters: {
    langUI(state) {
      return state.lang === 'en' ? 'ភាសាខ្មែរ' : 'English'
    },
    userFullName(state) {
      return state.currentUser ? state.currentUser.profile.fullName : 'Unknown'
    },
    allowedBranchOpts(state) {
      let opts = []
      if (state.allowedBranches) {
        state.allowedBranches.forEach(doc => {
          opts.push({ label: `${doc._id} : ${doc.name}`, value: doc._id, doc })
        })
      }

      return opts
    },
    currentBranchId(state) {
      return state.currentBranch ? state.currentBranch._id : null
    },
    userExpiryDay(state) {
      let expiryDay
      if (state.currentUser && state.currentUser.profile.expiryDate) {
        expiryDay = moment(state.currentUser.profile.expiryDate).diff(
          moment().startOf('day'),
          'days'
        )
      }

      return expiryDay
    },
    // Date format setting
    dateFormat(state) {
      let format = state.company
        ? state.company.setting.dateFormat.split(' ')
        : 'DD/MM/YYYY'
      if (format[0] === 'DD/MM/YYYY') {
        return 'dd/MM/yyyy'
      }
    },
    dateTimeFormat(state) {
      let format = state.company
        ? state.company.setting.dateFormat.split(' ')
        : 'DD/MM/YYYY'
      if (format[0] === 'DD/MM/YYYY') {
        return 'dd/MM/yyyy'
      }
    },
  },
  mutations: {
    UPDATE_COMPANY(state, value) {
      Session.setAuth('company', value)
      state.company = value
    },
    UPDATE_CURRENT_USER(state, value) {
      Session.setAuth('currentUser', value)
      state.currentUser = value
    },
    UPDATE_ALLOWED_BRANCHES(state, value) {
      Session.setAuth('allowedBranches', value)
      state.allowedBranches = value
    },
    UPDATE_CURRENT_BRANCH(state, value) {
      Session.setAuth('currentBranch', value)
      state.currentBranch = value
    },
    LOGOUT(state) {
      Session.clearAuth()
      state.currentUser = null
      router.push({ path: '/login' })
    },
  },
  actions: {
    updateCompany({ commit }, value) {
      commit('UPDATE_COMPANY', value)
    },
    login({ commit }, formData) {
      return new Promise((resolve, reject) => {
        Meteor.loginWithPassword(
          formData.username,
          formData.password,
          error => {
            if (error) {
              reject(error)
            } else {
              commit('UPDATE_CURRENT_USER', Meteor.user())
              resolve('success')
            }
          }
        )
      })
    },
    logout({ commit }) {
      Meteor.logout(() => {
        commit('LOGOUT')
      })
    },
    loadCurrentUser({ commit }) {
      // See https://forums.meteor.com/t/meteor-userid-returns-an-id-but-meteor-user-returns-undefined/18355/5

      //! Meteor.user() will not be ready right away,
      //! it is reactive so we need to wait for it
      Tracker.autorun(() => {
        if (Meteor.user()) {
          commit('UPDATE_CURRENT_USER', Meteor.user())
        }
      })
    },
    updateAllowedBranches({ commit }, branches) {
      commit('UPDATE_ALLOWED_BRANCHES', branches)
      commit(
        'UPDATE_CURRENT_BRANCH',
        branches && branches.length ? branches[0] : Session.get('currentBranch')
      )
    },
    updateCurrentBranch({ commit }, value) {
      commit('UPDATE_CURRENT_BRANCH', value)
    },
    // Map actions
    delView({ dispatch }, viewName) {
      return dispatch('tagView/delView', viewName).then(res => {
        return res
      })
    },
  },
  // Modules
  modules: {
    tagView,
  },
}

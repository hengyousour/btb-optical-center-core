import _ from 'lodash'

const homeView = {
  path: '/',
  name: 'Dashboard',
  meta: {
    title: 'Dashboard',
    icon: 'fas fa-tachometer-alt',
    noCache: true,
  },
}

export default {
  namespaced: true,
  state: {
    linkActive: {},
    visitedViews: [homeView],
    cachedViews: [],
  },
  mutations: {
    MAKE_LINK_ACTIVE(state, name) {
      if (name) {
        state.linkActive = {
          [name]: true,
        }
      } else {
        state.linkActive = {}
      }
    },
    ADD_VISITED_VIEW: (state, view) => {
      if (state.visitedViews.some(v => v.path === view.path)) return false
      state.visitedViews.push(view)

      // if (state.visitedViews.some(v => v.name === view.name)) {
      //   state.visitedViews = state.visitedViews.map(o => {
      //     if (o.name === view.name) {
      //       return view
      //     } else {
      //       return o
      //     }
      //   })
      // } else {
      //   state.visitedViews.push(view)
      // }
    },
    ADD_CACHED_VIEW: (state, view) => {
      if (state.cachedViews.includes(view.name)) return false
      if (!view.meta.noCache) {
        state.cachedViews.push(view.name)
      }
    },
    DEL_VISITED_VIEW: (state, view) => {
      for (const [i, v] of state.visitedViews.entries()) {
        if (v.path === view.path) {
          state.visitedViews.splice(i, 1)
          break
        }
      }
    },
    DEL_CACHED_VIEW: (state, view) => {
      for (const i of state.cachedViews) {
        if (i === view.name) {
          const index = state.cachedViews.indexOf(i)
          state.cachedViews.splice(index, 1)
          break
        }
      }
    },
    DEL_ALL_VISITED_VIEWS: state => {
      state.visitedViews = [homeView]
    },
    DEL_ALL_CACHED_VIEWS: state => {
      state.cachedViews = []
    },
  },
  actions: {
    makeLinkActive({ commit }, name) {
      commit('MAKE_LINK_ACTIVE', name)
    },
    addView({ dispatch }, view) {
      dispatch('addVisitedView', view)
      dispatch('addCachedView', view)
    },
    addVisitedView({ commit }, view) {
      commit('ADD_VISITED_VIEW', view)
    },
    addCachedView({ commit }, view) {
      commit('ADD_CACHED_VIEW', view)
    },
    delView({ dispatch, state, rootState }, view) {
      let route = view || rootState.route

      return new Promise(resolve => {
        dispatch('delVisitedView', route)
        dispatch('delCachedView', route)
        resolve({
          visitedViews: [...state.visitedViews],
          cachedViews: [...state.cachedViews],
        })
      })
    },
    delVisitedView({ commit, state }, view) {
      return new Promise(resolve => {
        commit('DEL_VISITED_VIEW', view)
        resolve([...state.visitedViews])
      })
    },
    delCachedView({ commit, state }, view) {
      return new Promise(resolve => {
        commit('DEL_CACHED_VIEW', view)
        resolve([...state.cachedViews])
      })
    },
    delAllViews({ dispatch, state }) {
      return new Promise(resolve => {
        dispatch('delAllVisitedViews')
        dispatch('delAllCachedViews')
        resolve({
          visitedViews: [...state.visitedViews],
          cachedViews: [...state.cachedViews],
        })
      })
    },
    delAllVisitedViews({ commit, state }) {
      return new Promise(resolve => {
        commit('DEL_ALL_VISITED_VIEWS')
        resolve([...state.visitedViews])
      })
    },
    delAllCachedViews({ commit, state }) {
      return new Promise(resolve => {
        commit('DEL_ALL_CACHED_VIEWS')
        resolve([...state.cachedViews])
      })
    },
  },
}

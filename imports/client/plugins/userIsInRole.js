import { Meteor } from 'meteor/meteor'
import { Session } from 'meteor/session'
import { Roles } from 'meteor/alanning:roles'
import _ from 'lodash'

const UserIsInRolePlugin = {
  install(Vue, options) {
    Vue.prototype.$userIsInRole = function(roles) {
      let currentUser = Meteor.user() || Session.get('currentUser')
      let userRoles = ['super']
      if (_.isString(roles)) {
        userRoles.push(roles)
      } else if (_.isArray(roles)) {
        userRoles = userRoles.concat(roles)
      }

      return Roles.userIsInRole(currentUser, userRoles)
    }

    // Vue.prototype.$userIsInRole2 = function(roles) {
    //   let currentUser = Meteor.user() || Session.get('currentUser')
    //   let userRoles = ['super']
    //   if (_.isString(roles)) {
    //     userRoles.push(roles)
    //   } else if (_.isArray(roles)) {
    //     userRoles = userRoles.concat(roles)
    //   }

    //   let result = _.intersection(currentUser.roles, userRoles)
    //   return result.length ? true : false
    // }
  },
}

export default UserIsInRolePlugin

import Msg from '/imports/client/lib/message'
import Notify from '/imports/client/lib/notify'

const restoreMixin = {
  methods: {
    $_restoreMixin({
      meteorMethod,
      selector,
      successMethod,
      successParams,
      loading,
      title,
    }) {
      selector = selector || {}
      successParams = successParams || {}
      loading = loading ? this[loading] : ''
      title = title ? ` [${title}]` : ''

      this.$confirm(`This will restore ${title}. Continue?`, 'Warning', {
        type: 'warning',
      })
        .then(() => {
          loading = true

          meteorMethod
            .callPromise(selector)
            .then(result => {
              loading = false
              successMethod ? this[successMethod](successParams) : ''
              Msg.success()
            })
            .catch(error => {
              loading = false
              Notify.error({ message: error })
            })
        })
        .catch(() => {
          Msg.warning('Your transaction is canceled')
        })
    },
  },
}

export default restoreMixin

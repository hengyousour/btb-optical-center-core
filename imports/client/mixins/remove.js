import Msg from '/imports/client/lib/message'
import Notify from '/imports/client/lib/notify'

const removeMixin = {
  methods: {
    $_removeMixin({
      meteorMethod,
      selector,
      successMethod,
      successParams,
      loading,
      title,
    }) {
      selector = selector || {}
      successParams = successParams || {}
      loading = loading ? this[loading] : ''
      title = title ? ` [${title}]` : ''

      this.$confirm(
        `Are you sure you would like to permanently delete ${title}?`,
        'Warning',
        {
          type: 'error',
        }
      )
        .then(() => {
          loading = true

          meteorMethod
            .callPromise(selector)
            .then(result => {
              loading = false
              successMethod ? this[successMethod](successParams) : ''
              Msg.success()
            })
            .catch(error => {
              loading = false
              Notify.error({ message: error })
            })
        })
        .catch(() => {
          Msg.warning('Your transaction is canceled')
        })
    },
  },
}

export default removeMixin

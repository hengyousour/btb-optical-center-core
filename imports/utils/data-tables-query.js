import _ from 'lodash'
import moment from 'moment'

export default function markDataTablesQuery(query, fieldType) {
  // Selector
  let selector = {}
  if (!_.isEmpty(query.filters)) {
    const fieldName = query.filters[0].vals[0]
    const fieldVal = query.filters[1].vals[0]
    if (fieldName && fieldVal) {
      // Check field type
      switch (fieldType[fieldName].name) {
        case 'Number':
          selector[query.name] = parseFloat(query.value)
          break
        case 'Date':
          selector[fieldName] = {
            $gte: moment(fieldVal, 'DD/MM/YYYY')
              .startOf('day')
              .toDate(),
            $lte: moment(fieldVal, 'DD/MM/YYYY')
              .endOf('day')
              .toDate(),
          }
          break
        case 'Boolean':
          if (fieldVal === 'true' || fieldVal === 'false') {
            selector[fieldName] = eval(fieldVal)
          }
          break
        case 'String':
          selector[fieldName] = { $regex: fieldVal, $options: 'i' }
          break
      }
    }
  }

  // Options
  const skip = (query.page - 1) * query.pageSize
  const limit = query.pageSize
  const sort = {
    [query.sortInfo.prop]: query.sortInfo.order === 'ascending' ? 1 : -1,
  }
  const options = { sort, skip, limit }
  console.log('DataTable: ', selector, options)

  return { selector, options }
}

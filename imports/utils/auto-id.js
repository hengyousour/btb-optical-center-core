import { check } from 'meteor/check'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'
import numeral from 'numeral'

import Counters from '../api/counters/counters'

const AutoId = {
  // make: function (collection, opts = {}) {
  //     _.defaults(opts, {
  //         field: '_id',
  //         length: 3,
  //         selector: {}
  //     });
  //
  //     let newId = _.padStart(1, opts.length, '0');
  //     let sort = {[opts.field]: -1};
  //
  //     let doc = collection.findOne(opts.selector, {sort});
  //
  //     if (doc) {
  //         let tmpId = parseInt(doc[opts.field]) + 1;
  //         // Check length
  //         if (tmpId.toString().length > opts.length) {
  //             newId = null;
  //         } else {
  //             newId = _.padStart(tmpId, opts.length, '0');
  //         }
  //     }
  //
  //     return newId;
  // },
  make: function(collection, opts = {}) {
    _.defaults(opts, {
      prefix: '',
      field: '_id',
      length: 3,
      selector: {},
    })

    let newId = opts.prefix + _.padStart(1, opts.length, '0')
    let sort = { [opts.field]: -1 }

    let selector = opts.selector
    if (opts.prefix) {
      selector[opts.field] = new RegExp(`^${opts.prefix}`, 'm')
    }

    let doc = collection.findOne(selector, { sort })
    if (doc) {
      let currentId = opts.prefix
        ? doc[opts.field].slice(-opts.length)
        : doc[opts.field]
      let tmpId = parseInt(currentId) + 1

      // Check length
      if (tmpId.toString().length > opts.length) {
        newId = null
      } else {
        newId = opts.prefix + _.padStart(tmpId, opts.length, '0')
      }
    }

    return newId
  },
  increase: function(collection, opts = {}) {
    _.defaults(opts, {
      field: 'code',
      length: 0,
      inc: 1,
      selector: {},
    })

    //lookup highest value
    let index = 0
    let sort = { [opts.field]: -1 }
    let doc = collection.findOne(opts.selector, { sort })
    if (doc) {
      // Extract numbers from a string
      let exNumber = doc[opts.field].match(/\d+/g)
      index = Number(_.last(exNumber))
    }
    index += opts.inc
    index = _.padStart(index, opts.length, '0')

    return index
  },
}

export default AutoId

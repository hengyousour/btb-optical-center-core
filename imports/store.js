import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

// Store
import app from '/imports/client/store'
import demo from '/imports/modules/demo/client/store/'

const store = new Vuex.Store({
  modules: {
    app,
    demo,
  },
})

export default store

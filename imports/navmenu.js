import { Meteor } from 'meteor/meteor'

//------ Module -----
import appSidebarMenu from '/imports/client/sidebarMenu'
import appReportMenu from '/imports/client/reportMenu'
import demoSidebarMenu from '/imports/modules/demo/client/sidebarMenu'
import demoReportMenu from '/imports/modules/demo/client/reportMenu'

let sidebarMenu = []
let reportMenu = []

// Dev mode
if (Meteor.isDevelopment) {
  sidebarMenu = sidebarMenu.concat(demoSidebarMenu)
  reportMenu = reportMenu.concat(demoReportMenu)
}
sidebarMenu = sidebarMenu.concat(appSidebarMenu)
reportMenu = reportMenu.concat(appReportMenu)

export { sidebarMenu, reportMenu }

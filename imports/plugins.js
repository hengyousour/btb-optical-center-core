import Vue from 'vue'

// Router
import VueRouter from 'vue-router'
Vue.use(VueRouter)

// Vuex
import Vuex from 'vuex'
Vue.use(Vuex)

// Element UI
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/en'
Vue.use(ElementUI, { locale, size: 'small' })

// Data table
import { DataTables, DataTablesServer } from 'vue-data-tables'
Vue.use(DataTables)
Vue.use(DataTablesServer)

// Tracker
import VueMeteorTracker from 'vue-meteor-tracker'
Vue.use(VueMeteorTracker)
Vue.config.meteor.freeze = true

// Vue Google Map
import VueGoogleMaps from 'vue-googlemaps'
import 'vue-googlemaps/dist/vue-googlemaps.css'
Vue.use(VueGoogleMaps, {
  load: {
    apiKey: 'AIzaSyBVH-aNNlZKmjRYxjt_2c0-m5AxYhF-iqU',
    libraries: ['places'],
  },
})

// Meta
import Meta from 'vue-meta'
Vue.use(Meta)

// NProgress
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

// Json Excel
// import JsonExcel from 'vue-json-excel';
// Vue.component('downloadExcel', JsonExcel);

// Font Awesome 5
import '@fortawesome/fontawesome-free/css/all.min.css'

// Animate CSS
import 'animate.css/animate.min.css'

// Local Plugin
// Check user is in roles
import UserIsInRolePlugin from '/imports/client/plugins/userIsInRole'
Vue.use(UserIsInRolePlugin)

import LocalFilters from '/imports/client/plugins/filters'
Vue.use(LocalFilters)

import './plugins'

import { Meteor } from 'meteor/meteor'
import Vue from 'vue'

// NProgress
import NProgress from 'nprogress'

// Router
import router from './router'

// Router Sync
import { sync } from 'vuex-router-sync'
import store from './store'
sync(store, router)

// App layout
import AppLayout from '/imports/client/layouts/AppLayout.vue'

Vue.config.productionTip = false

/******************
 * Meteor Startup
 *****************/
Meteor.startup(() => {
  // Before each
  router.beforeEach((to, from, next) => {
    NProgress.start()
    // console.log(`Before router`, store.state.app)

    if (to.meta.noAuth) {
      // Check user
      if (Meteor.userId()) {
        next({ path: '/' })
      } else {
        next()
      }
    } else {
      // Check user
      if (Meteor.userId()) {
        next()
      } else {
        next({ path: '/login' })
      }
    }
  })

  // // After each
  router.afterEach((to, from) => {
    NProgress.done()
  })

  // Start the vue app
  new Vue({
    router,
    store,
    ...AppLayout,
  }).$mount('app')
})

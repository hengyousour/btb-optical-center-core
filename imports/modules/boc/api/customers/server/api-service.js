import { CUSTOMERS } from '../methods'

JsonRoutes.add('get', '/find_customer/:selector/:options', function(
  req,
  res,
  next
) {
  res.charset = 'utf-8'
  const selector = req.params.selector ? JSON.parse(req.params.selector) : {}
  const options = req.params.options ? JSON.parse(req.params.options) : {}
  let data = {}
  data.result = CUSTOMERS.findCustomer(selector, options)
  data.code = '200'
  JsonRoutes.sendResult(res, {
    data: data,
  })
})

JsonRoutes.add('get', '/findOne_customer/:selector/:options', function(
  req,
  res,
  next
) {
  res.charset = 'utf-8'
  const selector = req.params.selector ? JSON.parse(req.params.selector) : {}
  const options = req.params.options ? JSON.parse(req.params.options) : {}
  let data = {}
  data.result = CUSTOMERS.findOneCustomer(selector, options)
  data.code = '200'
  JsonRoutes.sendResult(res, {
    data: data,
  })
})

JsonRoutes.add('post', '/insert_customer', function(req, res, next) {
  res.charset = 'utf-8'
  const doc = req.body
  CUSTOMERS.insertCustomer(doc, (error, result) => {
    let data = {}
    if (error) {
      data.code = '403'
      data.msg = error.message
      data.result = ''
    } else {
      data.code = '201'
      data.result = result
    }
    JsonRoutes.sendResult(res, {
      data: data,
    })
  })
})

JsonRoutes.add('put', '/update_customer/:selector/:options', function(
  req,
  res,
  next
) {
  res.charset = 'utf-8'
  const selector = req.params.selector ? JSON.parse(req.params.selector) : {}
  const modifier = req.body
  const options = req.params.options ? JSON.parse(req.params.options) : {}
  CUSTOMERS.updateCustomer(selector, modifier, options, (error, result) => {
    let data = {}
    if (error) {
      data.code = '403'
      data.msg = error.message
      data.result = ''
    } else {
      data.code = result ? '201' : '304'
      data.result = result
    }
    JsonRoutes.sendResult(res, {
      data: data,
    })
  })
})

JsonRoutes.add('put', '/update_customer_status/:selector', function(
  req,
  res,
  next
) {
  res.charset = 'utf-8'
  const selector = req.params.selector ? JSON.parse(req.params.selector) : {}

  CUSTOMERS.updateCustomerStatus(selector, (error, result) => {
    let data = {}
    if (error) {
      data.code = '403'
      data.msg = error.message
      data.result = ''
    } else {
      data.code = result ? '201' : '304'
      data.result = result
    }
    JsonRoutes.sendResult(res, {
      data: data,
    })
  })
})

JsonRoutes.add('delete', '/remove_customer/:selector', function(
  req,
  res,
  next
) {
  res.charset = 'utf-8'
  const selector = req.params.selector ? JSON.parse(req.params.selector) : {}

  CUSTOMERS.removeCustomer(selector, (error, result) => {
    let data = {}
    if (error) {
      data.code = '403'
      data.msg = error.message
      data.result = ''
    } else {
      data.code = result ? '201' : '304'
      data.result = result
    }
    JsonRoutes.sendResult(res, {
      data: data,
    })
  })
})

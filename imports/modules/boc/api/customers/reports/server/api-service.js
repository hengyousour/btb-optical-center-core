import customerReport from '../customer-report'
import customerOutstandingReport from '../customer-outstanding-report'

JsonRoutes.add('get', '/customer_report/:selector/:options', function(
  req,
  res,
  next
) {
  res.charset = 'utf-8'
  const selector = req.params.selector ? JSON.parse(req.params.selector) : {}
  const options = req.params.options ? JSON.parse(req.params.options) : {}
  let data = {}
  data.result = customerReport(selector, options)
  data.code = '200'
  JsonRoutes.sendResult(res, {
    data: data,
  })
})

JsonRoutes.add(
  'get',
  '/customer_outstanding_report/:selector/:options',
  function(req, res, next) {
    res.charset = 'utf-8'
    const selector = req.params.selector ? JSON.parse(req.params.selector) : {}
    const options = req.params.options ? JSON.parse(req.params.options) : {}
    let data = {}
    data.result = customerOutstandingReport(selector, options)
    data.code = '200'
    JsonRoutes.sendResult(res, {
      data: data,
    })
  }
)

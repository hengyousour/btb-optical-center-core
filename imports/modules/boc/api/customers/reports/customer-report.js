import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'
import moment from 'moment'

import { throwError } from '/imports/utils/security'
import rateLimit from '/imports/utils/rate-limit'
import getNextSeq from '/imports/utils/get-next-seq'

import Customers from '../customers'
import Company from '../../../../../api/company/company'
import Branch from '../../../../../api/branches/branches'


export default function customerReport(params, callback) {
  // return Customers.insert(doc, callback);
  if (Meteor.isServer) {

    let rptTitle, rptHeader, rptContent;

    let fDate = moment(params.fromDate, "DD/MM/YYYY")
      .startOf("day")
      .toDate();
    let tDate =
      moment(params.toDate, "DD/MM/YYYY")
        .endOf("day")
        .toDate();

    // --- Title ---
    rptTitle = Company.findOne();

    // --- Header ---
    // Branch
    let branchDoc = Branch.find({ _id: { $eq: params.branchId } });

    params.branchHeader = _.map(branchDoc.fetch(), function (val) {
      return `${val._id} : ${val.name}`;
    });

    // Exchange
    // let exchangeDoc = Exchange.findOne(params.exchangeId);
    // params.exchangeHeader = JSON.stringify(exchangeDoc.rates, null, ' ');

    rptHeader = params;

    // --- Content ---
    let selector = {};
    selector.branchId = { $eq: params.branchId };
    selector.date = { $gte: fDate, $lte: tDate };

    rptContent = Customers.aggregate([
      {
        $match: selector
      },
      {
        $sort: { date: -1 }
      }
    ]);

    return { rptTitle, rptHeader, rptContent };

  }

};



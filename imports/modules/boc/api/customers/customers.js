import { Mongo } from 'meteor/mongo'
import SimpleSchema from 'simpl-schema'

const Customers = new Mongo.Collection('customers')
Customers.schema = new SimpleSchema({
  name: {
    type: String
  },
  age: {
    type: Number
  },
  gender: {
    type: String
  },
  date: {
    type: Date
  },
  refer: {
    type: Object
  },
  'refer.cons': {
    type: String
  },
  'refer.sur': {
    type: String
  },
  va: {
    type: Object
  },
  'va.right': {
    type: String
  },
  'va.left': {
    type: String
  },
  rightEye: {
    type: Object
  },
  'rightEye.sph': {
    type: String
  },
  'rightEye.cyl': {
    type: String
  },
  'rightEye.axis': {
    type: String
  },
  'rightEye.va': {
    type: String
  },
  leftEye: {
    type: Object
  },
  'leftEye.sph': {
    type: String
  },
  'leftEye.cyl': {
    type: String
  },
  'leftEye.axis': {
    type: String
  },
  'leftEye.va': {
    type: String
  },
  add: {
    type: String
  },
  distancePd: {
    type: String
  },
  nearPd: {
    type: String
  },
  status: {
    type: String
  },
  branchId: {
    type: String,
    defaultValue: "001",
    // index: 1, // Or could set in `/startup/ensure-index.js`
  },
})

Customers.attachSchema(Customers.schema)
Customers.timestamp()

export default Customers

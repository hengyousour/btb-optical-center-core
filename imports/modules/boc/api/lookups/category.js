// import { Meteor } from 'meteor/meteor'
// import { ValidatedMethod } from 'meteor/mdg:validated-method'
// import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
// import SimpleSchema from 'simpl-schema'
// import _ from 'lodash'

// // Lib
// import rateLimit from '/imports/utils/rate-limit'

// // Collection
// import Categories from '../categories/Categories'

// // Post
// export const lookupCategory = new ValidatedMethod({
//   name: 'demo.lookupCategory',
//   mixins: [CallPromiseMixin],
//   validate: new SimpleSchema({
//     selector: {
//       type: Object,
//       blackbox: true,
//       optional: true,
//     },
//   }).validator(),
//   run({ selector }) {
//     if (Meteor.isServer) {
//       Meteor._sleepForMs(100)
//       selector = selector || {}
//       let sort = { name: 1 }

//       let list = []
//       let data = Categories.find(selector, { sort }).fetch()
//       data.forEach(o => {
//         let labelCustom = `
//                     <span class="custom-select-opts-left">${o.name}</span>
//                     <span class="custom-select-opts-right">${o.memo}</span>
//                     `
//         list.push({
//           label: o.name,
//           value: o._id,
//           labelCustom,
//         })
//       })

//       return list
//     }
//   },
// })

// rateLimit({
//   methods: [lookupCategory],
// })

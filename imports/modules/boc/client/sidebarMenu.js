const sidebarMenu = [
  {
    title: 'Post',
    icon: 'fas fa-dollar-sign',
    route: { name: 'PosCenter' },
    children: [
      {
        title: 'Make Post',
        route: { name: 'DemoPost' },
      },
    ],
  },
  {
    title: 'Demo Page',
    icon: 'fas fa-file',
    route: { name: 'DemoPage' },
  },
  {
    title: 'Demo Setting',
    icon: 'fas fa-cog',
    route: { name: 'DemoSetting' },
  },
]

export default sidebarMenu

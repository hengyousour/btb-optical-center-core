/**
 * Main
 */
// import '../../api/views'
// import '../../api/lookups'
// import '../../api/validations'
// import '../../api/reports'

/**
 * API
 */
// import '../../api/testing'

// Customers
import '../../api/customers/methods'
import '../../api/customers/server/hooks'
import '../../api/customers/server/publications'
import '../../api/customers/server/api-service'

import '../../api/customers/reports/server/api-service'
import '../../api/customers/reports/customer-report'
import '../../api/customers/reports/server/hooks'
import '../../api/customers/reports/server/publications'
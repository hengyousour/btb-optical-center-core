import { Meteor } from 'meteor/meteor'

// Collection
import Categories from '../../api/categories/Categories'

// Method
import { insertCategory } from '../../api/categories/methods'

Meteor.startup(function() {
  /**
   * Development
   */
  if (Meteor.isDevelopment) {
    // Category
    if (Categories.find().count() === 0) {
      const data = [{ name: 'Beer' }, { name: 'Wine' }, { name: 'Pur Water' }]

      data.forEach(doc => {
        insertCategory.run(doc)
      })
    }
  }

  /**
   * Production
   */
})

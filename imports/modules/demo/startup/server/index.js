import './collection-indexes'
import './register-util'
import './register-api'
import './fixtures'

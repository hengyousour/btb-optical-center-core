import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'

import { throwError } from '/imports/utils/security'
import rateLimit from '/imports/utils/rate-limit'
import getNextSeq from '/imports/utils/get-next-seq'
import { getNextRefServer } from '/imports/utils/get-next-ref'

import Categories from './Categories'

// Find
export const findCategories = new ValidatedMethod({
  name: 'demo.findCategories',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    options: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}

      return Categories.find(selector, options).fetch()
    }
  },
})

// Find One
export const findOneCategory = new ValidatedMethod({
  name: 'demo.findOneCategory',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Categories.findOne({ _id })
    }
  },
})

// Insert
export const insertCategory = new ValidatedMethod({
  name: 'demo.insertCategory',
  mixins: [CallPromiseMixin],
  validate: Categories.schema.validator(),
  run(doc) {
    if (Meteor.isServer) {
      const _id = getNextSeq({
        // Mandatory
        _id: 'demo_categories',
        // Optional
        opts: {
          seq: 1,
          // paddingType: 'start',
          // paddingLength: 6,
          // paddingChar: '0',
          // prefix: '',
          // toString: true, // default
        },
      })

      // Check refNo
      if (!doc.refNo) {
        doc.refNo = getNextRefServer({
          collectionName: 'demo_categories',
          opts: {
            field: 'refNo',
            selector: {},
            paddingType: 'start',
            paddingLength: 3,
            paddingChar: '0',
            prefix: '',
          },
        })
      }

      try {
        doc._id = _id
        return Categories.insert(doc)
      } catch (error) {
        // Decrement seq
        getNextSeq({
          _id: 'demo_categories',
          opts: { seq: -1 },
        })
        throwError(error)
      }
    }
  },
})

// Update
export const updateCategory = new ValidatedMethod({
  name: 'demo.updateCategory',
  mixins: [CallPromiseMixin],
  validate: _.clone(Categories.schema)
    .extend({
      _id: String,
    })
    .validator(),
  run(doc) {
    if (Meteor.isServer) {
      return Categories.update({ _id: doc._id }, { $set: doc })
    }
  },
})

// Soft remove
export const softRemoveCategory = new ValidatedMethod({
  name: 'demo.softRemoveCategory',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Categories.softRemove(_id)
    }
  },
})

// Restore
export const restoreCategory = new ValidatedMethod({
  name: 'demo.restoreCategory',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Categories.restore(_id)
    }
  },
})

// Remove
export const removeCategory = new ValidatedMethod({
  name: 'demo.removeCategory',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Categories.remove(_id)
    }
  },
})

rateLimit({
  methods: [
    findCategories,
    findOneCategory,
    insertCategory,
    updateCategory,
    softRemoveCategory,
    restoreCategory,
    removeCategory,
  ],
})

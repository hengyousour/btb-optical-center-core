// Setting
import Setting from './pages/Setting.vue'

// Data
import PostCenter from './pages/PostCenter.vue'
import PostForm from './pages/Post.vue'
import Map from './pages/Map.vue'
import DemoPage from './pages/Demo.vue'

// Report
import PostReport from './reports/Post.vue'
import SampleReport from './reports/Sample.vue'

const prefix = '/demo'
const routes = [
  /**
   * Setting
   */
  {
    path: '/setting/:active?',
    name: 'DemoSetting',
    component: Setting,
    meta: {
      title: 'Setting',
      noCache: true,
      breadcrumb: {
        parent: 'Dashboard',
      },
    },
  },
  /**
   * Pos Center
   */
  {
    path: '/post-center',
    name: 'DemoPostCenter',
    component: PostCenter,
    meta: {
      title: 'Post Center',
      noCache: true,
      breadcrumb: {
        parent: 'Dashboard',
      },
    },
  },
  // Post
  {
    path: '/post',
    name: 'DemoPost',
    component: PostForm,
    meta: {
      title: 'Post',
      linkActive: 'DemoPostCenter',
      breadcrumb: {
        parent: 'DemoPostCenter',
      },
    },
  },
  // Map
  {
    path: '/map',
    name: 'DemoMap',
    component: Map,
    meta: {
      title: 'Map',
      linkActive: 'DemoPostCenter',
      noCache: true,
      breadcrumb: {
        parent: 'DemoPostCenter',
      },
    },
  },
  /**
   * Demo
   */
  {
    path: '/demo',
    name: 'DemoPage',
    component: DemoPage,
    meta: {
      title: 'Demo Page',
      breadcrumb: {
        parent: 'Dashboard',
      },
    },
  },
  /**
   * Report
   */
  // Sample
  {
    path: '/report-sample',
    name: 'DemoSampleReport',
    component: SampleReport,
    meta: {
      title: 'Sample Report',
      breadcrumb: {
        parent: 'Dashboard',
      },
    },
  },
  // Post
  {
    path: '/report-post',
    name: 'DemoPostReport',
    component: PostReport,
    meta: {
      title: 'Post Report',
      breadcrumb: {
        parent: 'Dashboard',
      },
    },
  },
].map(route => {
  route.path = prefix + route.path
  return route
})

export default routes

const reportMenu = [
  {
    title: 'Demo',
    icon: 'fas fa-democrat',
    activeGroups: ['General', 'Summary'],
    data: [
      // General
      {
        groupTitle: 'General',
        icon: 'fas fa-print',
        items: [
          {
            title: 'Post',
            route: { name: 'DemoPostReport' },
            memo: ``,
          },
          {
            title: 'Sample',
            route: { name: 'DemoSampleReport' },
            memo: ``,
          },
        ],
      },
      // Summary
      {
        groupTitle: 'Summary',
        icon: 'fas fa-print',
        items: [
          {
            title: 'Sample',
            route: { name: 'DemoSampleReport' },
            memo: ``,
          },
          {
            title: 'Sample',
            route: { name: 'DemoSampleReport' },
            memo: ``,
          },
        ],
      },
    ],
  },
]

export default reportMenu

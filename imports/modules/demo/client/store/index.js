// State
import moduleA from './moduleA'

export default {
  namespaced: true,
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  // Modules
  modules: {
    moduleA,
    // B, C...
  },
}

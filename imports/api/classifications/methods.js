import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'

import rateLimit from '/imports/utils/rate-limit'
import getNextSeq from '../../utils/get-next-seq'

import Classifications from './classifications'
import { throwError } from '../../utils/security'

// Find
export const findClassifications = new ValidatedMethod({
  name: 'app.findClassifications',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    options: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}

      return Classifications.find(selector, options).fetch()
    }
  },
})

// Find One
export const findOneClassification = new ValidatedMethod({
  name: 'app.findOneClassification',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    options: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}

      return Classifications.findOne(selector, options)
    }
  },
})

// Insert
export const insertClassification = new ValidatedMethod({
  name: 'app.insertClassification',
  mixins: [CallPromiseMixin],
  validate: Classifications.schema.validator(),
  run(doc) {
    if (Meteor.isServer) {
      const seq = getNextSeq({
        // Mandatory
        _id: 'classifications',
        // Optional
        opts: {
          seq: 1,
          // paddingType: 'start',
          // paddingLength: 6,
          // paddingChar: '0',
          // prefix: '',
          // toString: true, // default
        },
      })

      try {
        doc._id = seq
        return Classifications.insert(doc)
      } catch (error) {
        getNextSeq({ _id: 'classifications', opts: { seq: -1 } })
        throwError(error)
      }
    }
  },
})

// Update
export const updateClassification = new ValidatedMethod({
  name: 'app.updateClassification',
  mixins: [CallPromiseMixin],
  validate: _.clone(Classifications.schema)
    .extend({
      _id: String,
    })
    .validator(),
  run(doc) {
    if (Meteor.isServer) {
      return Classifications.update({ _id: doc._id }, { $set: doc })
    }
  },
})

/**
 * Upsert methods
 */
export const upsertClassification = new ValidatedMethod({
  name: 'app.upsertClassification',
  mixins: [CallPromiseMixin],
  validate: null,
  run(doc) {
    if (Meteor.isServer) {
      let _id
      if (!doc._id) {
        _id = getNextSeq({
          _id: 'classifications',
          opts: {
            seq: 1,
          },
        })
        doc._id = _id.toString()
      }
      try {
        return Classifications.upsert({ _id: doc._id }, { $set: doc })
      } catch (error) {
        if (_id) {
          _id = getNextSeq({
            _id: 'classifications',
            opts: { seq: -1 },
          })
        }
        throwError(error)
      }
    }
  },
})

// Soft remove
export const softRemoveClassification = new ValidatedMethod({
  name: 'app.softRemoveClassification',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Classifications.softRemove({ _id })
    }
  },
})

// Restore
export const restoreClassification = new ValidatedMethod({
  name: 'app.restoreClassification',
  mixins: [CallPromiseMixin],
  // validate: null,
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Classifications.restore({ _id })
    }
  },
})

// Remove
export const removeClassification = new ValidatedMethod({
  name: 'app.removeClassification',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Classifications.remove({ _id })
    }
  },
})

rateLimit({
  methods: [
    findClassifications,
    findOneClassification,
    insertClassification,
    upsertClassification,
    updateClassification,
    softRemoveClassification,
    restoreClassification,
    removeClassification,
  ],
})

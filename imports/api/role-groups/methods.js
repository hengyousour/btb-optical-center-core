import { Meteor } from 'meteor/meteor'
// import { Accounts } from 'meteor/accounts-base'
import { check } from 'meteor/check'
// import { Roles } from 'meteor/alanning:roles'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'

import { throwError } from '../../utils/security'
import rateLimit from '../../utils/rate-limit'
import getNextSeq from '/imports/utils/get-next-seq'
import RoleGroups from './RoleGroups'

// Find
export const findRoleGroups = new ValidatedMethod({
  name: 'app.findRoleGroups',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    options: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}

      return RoleGroups.find(selector, options).fetch()
    }
  },
})

// Find One
export const findOneRoleGroup = new ValidatedMethod({
  name: 'app.findOneRoleGroup',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    options: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}
      return RoleGroups.findOne(selector, options)
    }
  },
})

// Check group is used
export const checkRoleGroupIsUsed = new ValidatedMethod({
  name: 'app.checkRoleGroupIsUsed',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    options: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}
      return Meteor.users.findOne(selector, options)
    }
  },
})

// Insert
export const insertRoleGroup = new ValidatedMethod({
  name: 'app.insertRoleGroup',
  mixins: [CallPromiseMixin],
  validate: RoleGroups.schema.validator(),
  run(doc) {
    if (Meteor.isServer) {
      Meteor._sleepForMs(300)
      // console.log('insertFund -> doc', doc)
      const _id = getNextSeq({
        // Mandatory
        filter: {
          _id: 'app_roleGroups',
          // type: '001' // BranchId
        },
        // Optional
        opts: {
          seq: 1,
          // paddingType: 'start',
          // paddingLength: 6,
          // paddingChar: '0',
          // prefix: ''
        },
      })
      try {
        doc._id = _id.toString()
        return RoleGroups.insert(doc)
      } catch (error) {
        // Decrement seq
        getNextSeq({
          filter: { _id: 'app_roleGroups' },
          opts: { seq: -1 },
        })
        throwError(error)
      }
    }
  },
})

// Update
export const updateRoleGroup = new ValidatedMethod({
  name: 'app.updateRoleGroup',
  mixins: [CallPromiseMixin],
  validate: _.clone(RoleGroups.schema)
    .extend({
      _id: String,
    })
    .validator(),
  run(doc) {
    if (Meteor.isServer) {
      let roles = { [doc._id]: doc.roles }
      Meteor.users.update(
        { 'profile.rolesGroup': doc._id },
        {
          $set: {
            roles: roles,
          },
        },
        { multi: true }
      )
      return RoleGroups.update({ _id: doc._id }, { $set: doc })
    }
  },
})

export const removeRoleGroup = new ValidatedMethod({
  name: 'app.removeRoleGroup',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: { type: String },
  }).validator(),
  run(selector) {
    if (Meteor.isServer) {
      RoleGroups.remove(selector)
      return 'success'
    }
  },
})

rateLimit({
  methods: [
    findRoleGroups,
    findOneRoleGroup,
    insertRoleGroup,
    updateRoleGroup,
    removeRoleGroup,
  ],
})

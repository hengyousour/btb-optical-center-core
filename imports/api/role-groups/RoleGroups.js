import { Mongo } from 'meteor/mongo'
import SimpleSchema from 'simpl-schema'

const RoleGroups = new Mongo.Collection('app_roleGroups')

// Schema
RoleGroups.schema = new SimpleSchema({
  name: {
    type: String,
  },
  roles: {
    type: Array,
  },
  'roles.$': {
    type: String,
  },
  status: {
    type: String,
  },
})

RoleGroups.attachSchema(RoleGroups.schema)
RoleGroups.timestamp()

export default RoleGroups

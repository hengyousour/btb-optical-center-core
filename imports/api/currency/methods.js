import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'

import rateLimit from '/imports/utils/rate-limit'

import Currency from './currency'

// Find
export const findCurrency = new ValidatedMethod({
  name: 'app.findCurrency',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({}).validator(),
  run() {
    if (Meteor.isServer) {
      return Currency.find().fetch()
    }
  },
})

// Find One
export const findOneCurrency = new ValidatedMethod({
  name: 'app.findOneCurrency',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: {
      type: String,
    },
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Currency.findOne({ _id })
    }
  },
})

rateLimit({
  methods: [findCurrency, findOneCurrency],
})

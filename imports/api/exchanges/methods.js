import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'
import moment from 'moment'

import rateLimit from '/imports/utils/rate-limit'
import getNextSeq from '/imports/utils/get-next-seq'
import { throwError } from '/imports/utils/security'
import Exchanges from './exchanges'

// Find
export const findExchanges = new ValidatedMethod({
  name: 'app.findExchanges',
  mixins: [CallPromiseMixin],
  validate: null,
  run(selector, options) {
    if (!this.isSimulation) {
      selector = selector || {}
      options = options || {}

      return Exchanges.find(selector, options).fetch()
    }
  },
})

// Find One
export const findOneExchange = new ValidatedMethod({
  name: 'app.findOneExchange',
  mixins: [CallPromiseMixin],
  // validate: Exchanges.schema.validator(),
  validate: null,
  run(selector, options) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}

      return Exchanges.findOne(selector, options)
    }
  },
})

// Insert
export const insertExchange = new ValidatedMethod({
  name: 'app.insertExchange',
  mixins: [CallPromiseMixin],
  validate: null,
  run(doc) {
    if (Meteor.isServer) {
      return Exchanges.insert(doc)
    }
  },
})

// Update
export const updateExchange = new ValidatedMethod({
  name: 'app.updateExchange',
  mixins: [CallPromiseMixin],
  // validate: Exchanges.schema.validator(),
  validate: null,
  run(doc) {
    if (Meteor.isServer) {
      return Exchanges.update({ _id: doc._id }, { $set: doc })
    }
  },
})

/**
 * Upsert methods
 */
export const upsertExchange = new ValidatedMethod({
  name: 'app.upsertExchange',
  mixins: [CallPromiseMixin],
  validate: null,
  run(doc) {
    if (Meteor.isServer) {
      let _id
      if (!doc._id) {
        _id = getNextSeq({
          _id: 'exchanges',
          opts: {
            seq: 1,
          },
        })
        doc._id = _id.toString()
      }
      try {
        return Exchanges.upsert({ _id: doc._id }, { $set: doc })
      } catch (error) {
        if (_id) {
          _id = getNextSeq({
            _id: 'exchanges',
            opts: { seq: -1 },
          })
        }
        throwError(error)
      }
    }
  },
})
// Remove
export const removeExchange = new ValidatedMethod({
  name: 'app.removeExchange',
  mixins: [CallPromiseMixin],
  // validate: null,
  validate: new SimpleSchema({
    _id: { type: String },
  }).validator(),
  run(selector) {
    if (Meteor.isServer) {
      Exchanges.remove(selector)
      return 'success'
    }
  },
})

rateLimit({
  methods: [
    findExchanges,
    findOneExchange,
    insertExchange,
    updateExchange,
    removeExchange,
  ],
})

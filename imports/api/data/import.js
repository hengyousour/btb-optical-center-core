import { Meteor } from 'meteor/meteor'
import { EJSON } from 'meteor/ejson'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'

import rateLimit from '/imports/utils/rate-limit'

import FilesCollection from '/imports/api/files/files'
import importEmployee from './employee'
import importChartAccount from './chartAccount'
import importAccountType from './accountType'

export const importData = new ValidatedMethod({
  name: 'importData',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    importType: String,
    fileName: String,
  }).validator(),
  run({ importType, fileName }) {
    if (Meteor.isServer) {
      Meteor._sleepForMs(100)

      try {
        // Check import type
        const sourceFile = `/data/file_uploads/${fileName}.xlsx`
        let result

        switch (importType) {
          case 'Employee':
            result = importEmployee(sourceFile)
            break
          case 'AccountType':
            result = importAccountType(sourceFile)
            break
          case 'ChartOfAccount':
            result = importChartAccount(sourceFile)
            break
        }

        // Remove upload file
        FilesCollection.remove({ _id: fileName })

        return result
      } catch (error) {
        console.log('Import Error:', error)

        // Remove upload file
        FilesCollection.remove({ _id: fileName })
        throw new Meteor.Error(
          'Import Error',
          error.reason,
          EJSON.stringify(error.details)
        )
      }
    }
  },
})

rateLimit({
  methods: [importData],
})

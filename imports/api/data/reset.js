import { Meteor } from 'meteor/meteor'
import { EJSON } from 'meteor/ejson'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'

import rateLimit from '/imports/utils/rate-limit'

import { removeEmployees } from '../employees/methods'
import { removeAccountTypes } from '../account-types/methods'
import { removeChartAccounts } from '../chart-accounts/methods'

export const resetData = new ValidatedMethod({
  name: 'resetData',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    branchId: String,
    resetType: String,
  }).validator(),
  run({ branchId, resetType }) {
    if (Meteor.isServer) {
      Meteor._sleepForMs(100)

      try {
        // Check reset type
        let result

        switch (resetType) {
          case 'Employee':
            result = removeEmployees.run({
              selector: {
                branchId: branchId,
              },
            })
            break
          case 'AccountType':
            result = removeAccountTypes.run({})
            break
          case 'ChartOfAccount':
            result = removeChartAccounts.run({})
            break
        }

        return result
      } catch (error) {
        console.log('Reset Error:', error)

        throw new Meteor.Error(
          'Reset Error',
          error.reason,
          EJSON.stringify(error.details)
        )
      }
    }
  },
})

rateLimit({
  methods: [resetData],
})

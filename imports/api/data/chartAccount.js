import { Meteor } from 'meteor/meteor'
import _ from 'lodash'
import excelToJson from 'convert-excel-to-json'

import AccountTypes from '../account-types/account-types'
import ChartAccounts from '../chart-accounts/chart-accounts'
import { insertChartAccount } from '../chart-accounts/methods'

const importChartAccount = sourceFile => {
  let rowNo

  try {
    const jsonData = excelToJson({
      sourceFile,
      header: {
        rows: 1,
      },
      sheets: [
        {
          name: 'chart_account',
          columnToKey: {
            // code, name, parent, accountType, status, memo
            '*': '{{columnHeader}}',
          },
        },
      ],
    })

    jsonData.chart_account.forEach((o, index) => {
      rowNo = index + 2

      // Get account type id
      o.accountTypeId = AccountTypes.findOne({ name: o.accountType })._id

      // Check parent
      if (o.parent) {
        o.parent = ChartAccounts.findOne({ name: o.parent })._id
      }

      insertChartAccount.run(o)
    })

    return jsonData.chart_account.length
  } catch (error) {
    throw new Meteor.Error('Import Chart Account', rowNo, error)
  }
}

export default importChartAccount

import { Meteor } from 'meteor/meteor'
import excelToJson from 'convert-excel-to-json'

import { insertAccountType } from '../account-types/methods'

const importAccountType = sourceFile => {
  let rowNo

  try {
    const jsonData = excelToJson({
      sourceFile,
      header: {
        rows: 1,
      },
      sheets: [
        {
          name: 'account_type',
          columnToKey: {
            // code, name, nature, status, memo
            '*': '{{columnHeader}}',
          },
        },
      ],
    })

    jsonData.account_type.forEach((o, index) => {
      rowNo = index + 2
      insertAccountType.run(o)
    })

    return jsonData.account_type.length
  } catch (error) {
    throw new Meteor.Error('Import Account Type', rowNo, error)
  }
}

export default importAccountType

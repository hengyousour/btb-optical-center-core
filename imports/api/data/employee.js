import { Meteor } from 'meteor/meteor'
import _ from 'lodash'
import excelToJson from 'convert-excel-to-json'

import { insertEmployee } from '../employees/methods'

const importEmployee = sourceFile => {
  let rowNo

  try {
    const jsonData = excelToJson({
      sourceFile,
      header: {
        rows: 1,
      },
      sheets: [
        {
          name: 'employee',
          columnToKey: {
            // code, name, gender, role, linkUser, address, phone, email, status, branchId
            '*': '{{columnHeader}}',
          },
        },
      ],
    })

    jsonData.employee.forEach((o, index) => {
      rowNo = index + 2
      // Check contact
      o.contact = {
        address: o.address,
        telephone: o.telephone || '',
        email: o.email || '',
      }
      _.omit(o, ['address', 'telephone', 'email'])

      insertEmployee.run(o)
    })

    return jsonData.employee.length
  } catch (error) {
    throw new Meteor.Error('Import Employee', rowNo, error)
  }
}

export default importEmployee

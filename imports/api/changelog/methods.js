import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'

// Find

export const getChangelogMd = new ValidatedMethod({
  name: 'app.getChangelog',
  mixins: [CallPromiseMixin],
  validate: null,
  run() {
    if (Meteor.isServer) {
      return Assets.getText('CHANGELOG.md')
    }
  },
})

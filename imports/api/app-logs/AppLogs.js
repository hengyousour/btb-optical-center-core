import { Mongo } from 'meteor/mongo'
import SimpleSchema from 'simpl-schema'

const AppLogs = new Mongo.Collection('app_logs')

// Schema
AppLogs.schema = new SimpleSchema({
  // Page Title: App.User
  title: {
    type: String,
  },
  // 'CREATE', 'READ', 'UPDATE', 'DELETE', 'VIEW'
  level: {
    type: String,
    allowedValues: ['CREATE', 'READ', 'UPDATE', 'DELETE', 'VIEW'],
  },
  data: {
    type: Object,
    blackbox: true,
  },
  createdAt: {
    type: Date,
  },
  createdBy: {
    type: String,
    optional: true,
  },
})

AppLogs.attachSchema(AppLogs.schema)

export default AppLogs

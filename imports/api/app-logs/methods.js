import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'
import moment from 'moment'

import AppLogsCollection from './AppLogs'

// Insert
export const _insertAppLog = new ValidatedMethod({
  name: 'app._insertAppLog',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    // Page Title: App.User
    title: {
      type: String,
    },
    // 'CREATE', 'READ', 'UPDATE', 'DELETE', 'VIEW'
    level: {
      type: String,
      allowedValues: ['CREATE', 'READ', 'UPDATE', 'DELETE', 'VIEW'],
    },
    data: {
      type: Object,
      blackbox: true,
    },
  }).validator(),
  run(doc) {
    if (Meteor.isServer) {
      doc.createdAt = moment().toDate()
      // doc.createdBy = Meteor.userId() || ''
      return AppLogsCollection.insert(doc)
    }
  },
})

// Export
const AppLog = {
  create(doc) {
    doc.level = 'CREATE'
    return _insertAppLog.run(doc)
  },
  read(doc) {
    doc.level = 'READ'
    return _insertAppLog.run(doc)
  },
  update(doc) {
    doc.level = 'UPDATE'
    return _insertAppLog.run(doc)
  },
  delete(doc) {
    doc.level = 'DELETE'
    return _insertAppLog.run(doc)
  },
  view(doc) {
    doc.level = 'VIEW'
    return _insertAppLog.run(doc)
  },
}

export default AppLog

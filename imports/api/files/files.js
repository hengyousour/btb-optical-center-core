import { Meteor } from 'meteor/meteor'
import { FilesCollection } from 'meteor/ostrio:files'

const Files = new FilesCollection({
  // debug: true,
  storagePath: () => {
    // https://github.com/VeliovGroup/Meteor-Files/issues/651
    // mkdir -p /data/file_uploads
    // chmod -R 777 /data
    // Server path
    return '/data/file_uploads' // Mac, Ubuntu
    // return 'C:\\data\\file_uploads' // Windows

    // return `${Meteor.rootPath}/file_uploads` // .meteor/local/programs/server/
    // return `${process.env.PWD}/file_uploads` // app path
    // return `${Meteor.absolutePath}/file_uploads` // app path
  },
  collectionName: 'app_files',
  // permissions: 777,
  // parentDirPermissions: 777,
})

export default Files

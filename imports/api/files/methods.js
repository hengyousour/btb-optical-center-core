import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'

import rateLimit from '/imports/utils/rate-limit'

import Files from './files'

export const getFiles = new ValidatedMethod({
  name: 'app.getFiles',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      optional: true,
      blackbox: true,
    },
    options: {
      type: Object,
      optional: true,
      blackbox: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      Meteor._sleepForMs(300)
      selector = selector || {}
      options = options || {}

      return Files.find(selector, options).fetch()
    }
  },
})

export const getFileById = new ValidatedMethod({
  name: 'app.getFileById',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    id: String,
  }).validator(),
  run({ id }) {
    if (Meteor.isServer) {
      Meteor._sleepForMs(300)
      let file = Files.findOne({ _id: id })
      return file
    }
  },
})

rateLimit({
  methods: [],
})

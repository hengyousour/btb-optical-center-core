import Files from '../files'

Files.allow({
  insert() {
    return true
  },
  update() {
    return true
  },
  remove() {
    return true
  },
})

import { Meteor } from 'meteor/meteor'
import { Mongo } from 'meteor/mongo'

const ChartAccountView = new Mongo.Collection('app_chartAccountsView')
export default ChartAccountView

if (Meteor.isServer) {
  const db = ChartAccountView.rawDatabase()

  // Drop before create
  db.dropCollection('app_chartAccountsView', (err, res) => {
    db.createCollection('app_chartAccountsView', {
      viewOn: 'app_chartAccounts',
      pipeline: [
        {
          $lookup: {
            from: 'app_accountTypes',
            localField: 'accountTypeId',
            foreignField: '_id',
            as: 'accountTypeDoc',
          },
        },
        { $unwind: '$accountTypeDoc' },
      ],
    })
  })
}

import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'
import perfy from 'perfy'

import rateLimit from '/imports/utils/rate-limit'

import { getChartAccounts } from '../lib/getChartAccounts'

const reportAccountList = new ValidatedMethod({
  name: 'app.reportAccountList',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    accountType: {
      type: Array,
    },
    'accountType.$': {
      type: String,
    },
  }).validator(),
  run({ accountType }) {
    if (Meteor.isServer) {
      perfy.start('report')

      let selector = {}
      if (accountType && accountType.length) {
        selector.accountTypeId = { $in: accountType }
      }
      let data = getChartAccounts.run({ selector })

      return {
        data,
        exTime: perfy.end('report').time,
      }
    }
  },
})

rateLimit({
  methods: [reportAccountList],
})

export default reportAccountList

import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'
import moment, { now } from 'moment'

import rateLimit from '/imports/utils/rate-limit'

import { getChartAccounts } from '../lib/getChartAccounts'
import { getRetainedEarningsBeginning } from '../lib/getRetainedEarningsBeginning'
import { getNetIncome } from '../lib/getNetIncome'
import JournalsView from '../views/Journals'
import AccountType from '../account-types/account-types'
import Company from '../company/company'

const reportBalanceSheet = new ValidatedMethod({
  name: 'app.reportBalanceSheet',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    branchId: {
      type: Array,
      optional: true,
    },
    'branchId.$': String,
    currency: String,
    // showRowStatus: String,
    reportDate: Date,
  }).validator(),
  run(params) {
    if (Meteor.isServer) {
      const setting = Company.findOne().setting

      let fiscalDate = moment(params.asOfDate)
      fiscalDate.date(moment(setting.fiscalDate).format('DD'))
      fiscalDate.month(moment(setting.fiscalDate).format('MM'))
      fiscalDate = fiscalDate.toDate()

      const reportDate = moment(params.reportDate)
        .endOf('day')
        .toDate()

      // Get account type
      const accountType = AccountType.find({}).fetch()

      // Asset, Liability and Equity Selector
      let journalFilter = {
        tranDate: {
          $lte: reportDate,
        },
        currency: params.currency,
      }
      if (params.branchId.length) {
        journalFilter.branchId = { $in: params.branchId }
      }

      // Get journal
      let journals = JournalsView.aggregate([
        {
          $match: journalFilter,
        },
        {
          $unwind: '$details',
        },
        {
          $group: {
            _id: '$details.chartAccountId',
            totalDr: { $sum: '$details.dr' },
            totalCr: { $sum: '$details.cr' },
            totalAmount: { $sum: '$details.amount' },
          },
        },
      ])

      // Get retained earning
      let beginningRE = JournalsView.aggregate([
        {
          $match: journalFilter,
        },
        {
          $unwind: '$details',
        },
        {
          $match: {
            'details.accountTypeDoc.name': 'Retained Earnings',
          },
        },
        {
          $project: {
            chartAccountId: '$details.chartAccountId',
            details: 1,
            // currency: '$currency',
            amount: '$details.amount',
            // baseCurrency: baseCurrencyProjection,
          },
        },
        {
          $group: {
            _id: '$chartAccountId',
            // accountTypeDoc: { $last: '$details.accountTypeDoc' },
            total: { $sum: '$amount' },
          },
        },
      ])[0]

      // Get beginning net income
      let beginningNetIncome = getNetIncome.run({
        asOfDate: fiscalDate,
        type: 'Beginning',
        currency: [params.currency],
        branchId: params.branchId,
      })

      // Get chart account
      let data = []
      let chartAccounts = getChartAccounts.run({
        selector: {
          'accountTypeDoc.nature': { $in: ['Asset', 'Liability', 'Equity'] },
        },
        newTotalRow: true,
      })

      chartAccounts.forEach(chart => {
        // Parent
        if (chart.isParent) {
          if (chart.totalOf) {
            chart.totalAmount = _.sumBy(data, item => {
              if (item.parent == chart.totalOf && item.totalOf) {
                return item.totalAmount
              }
            })
          }
          data.push(chart)
        } else {
          // Get tran
          let trans = _.find(journals, { _id: chart._id })
          chart.totalAmount = 0
          if (trans && chart.accountTypeDoc.name != 'Retained Earnings') {
            chart.totalAmount = trans.totalAmount
            // data.push(chart)

            // // Create total
            // let childTotal = _.clone(chart)
            // childTotal.totalAmount = chart.totalAmount
            // childTotal.totalOf = childTotal._id
            // childTotal.order = childTotal.order + '___'
            // data.push(childTotal)
          }

          // Retained Earnings
          if (chart.accountTypeDoc.name === 'Retained Earnings') {
            let retainEarningAmount =
              chart._id === trans && trans._id ? trans.totalAmount : 0

            chart.totalAmount =
              (beginningRE &&
                beginningRE.total + beginningNetIncome + retainEarningAmount) ||
              0
          }
          data.push(chart)

          // Create total
          let childTotal = _.clone(chart)
          childTotal.totalAmount = chart.totalAmount
          childTotal.totalOf = childTotal._id
          childTotal.order = childTotal.order + '___'
          data.push(childTotal)

          //
        }
      })
      // Sort
      data = _.sortBy(data, ['order'])
      // console.log(chartAccounts)

      // Data
      let assetData = []
      let liabilityData = []
      let equityData = []

      accountType.forEach(p => {
        let accData = []
        p.indent = 0
        p.accountType = true

        // Check nature account
        if (p.nature === 'Asset') {
          assetData.push(p)
          accData = assetData
        } else if (p.nature === 'Liability') {
          liabilityData.push(p)
          accData = liabilityData
        } else if (p.nature === 'Equity') {
          // if (p.name != 'Retained Earnings')
          equityData.push(p)
          accData = equityData
        }

        // Loop data
        data.forEach(s => {
          if (p._id === s.accountTypeId) {
            accData.push(s)
          }
        })

        // Create total
        let childTotal = _.clone(p)
        let totalAmount = 0
        totalAmount = _.sumBy(accData, item => {
          if (
            item &&
            item.accountTypeDoc &&
            item.accountTypeDoc.name === p.name &&
            !item.totalOf
          ) {
            return item.totalAmount
          }
        })
        childTotal.totalAmount = totalAmount
        childTotal.totalOf = childTotal._id
        childTotal.totalType = true

        // if (p.name != 'Retained Earnings')
        accData.push(childTotal)

        // Check total amount to reject
        // if (!totalAmount) {
        //   accData = _.reject(accData, o => {
        //     return o._id === p._id && o.accountType
        //   })
        // }
        /**
         * Check account type
         * and assign value array
         */
        if (p.nature === 'Asset') {
          assetData = accData
        } else if (p.nature === 'Liability') {
          liabilityData = accData
        } else if (p.nature === 'Equity') {
          equityData = accData
        }
      })

      //Current net income
      const NetIncome = getNetIncome.run({
        asOfDate: reportDate,
        type: 'Current',
        currency: [params.currency],
        branchId: params.branchId,
      })

      // Push net income to equity array
      equityData.splice(equityData.length, 0, {
        _id: 'income',
        name: 'Net Income',
        indent: 0,
        totalAmount: NetIncome,
      })
      // Sum total
      let totalAsset = _.sumBy(assetData, item => {
        if (!item.totalOf) {
          return item.totalAmount
        }
      })
      let totalLiability = _.sumBy(liabilityData, item => {
        if (!item.totalOf) {
          return item.totalAmount
        }
      })

      let totalEquity = _.sumBy(equityData, item => {
        if (!item.totalOf) {
          return item.totalAmount
        }
      })

      return [
        { name: 'Assets', details: assetData, total: totalAsset },
        {
          name: 'Liabilities',
          details: liabilityData,
          total: totalLiability,
        },
        {
          name: 'Equities',
          details: equityData,
          total: totalEquity,
          totalAll: totalEquity + totalLiability,
        },
      ]
    }
  },
})

rateLimit({
  methods: [reportBalanceSheet],
})

export default reportBalanceSheet

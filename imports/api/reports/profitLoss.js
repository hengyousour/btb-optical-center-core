import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'
import moment from 'moment'

import rateLimit from '/imports/utils/rate-limit'

import { getChartAccounts } from '../lib/getChartAccounts'
import JournalsView from '../views/Journals'
import { getNetIncome } from '../lib/getNetIncome'

const reportProfitLoss = new ValidatedMethod({
  name: 'app.reportProfitLoss',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    branchId: Array,
    'branchId.$': String,
    currency: String,
    transactionType: Array,
    'transactionType.$': String,
    reportPeriod: Array,
    'reportPeriod.$': Date,
  }).validator(),
  run(params) {
    if (Meteor.isServer) {
      const dateF = moment(params.reportPeriod[0])
        .startOf('day')
        .toDate()

      const dateT = moment(params.reportPeriod[1])
        .endOf('day')
        .toDate()

      let match = {
        tranDate: { $lte: dateT, $gte: dateF },
        currency: { $eq: params.currency },
        'details.accountTypeDoc.nature': { $in: ['Revenue', 'Expense'] },
      }

      if (!_.isEmpty(params.branchId)) {
        match.branchId = {
          $in: params.branchId,
        }
      }

      let journals = JournalsView.aggregate([
        { $unwind: { path: '$details', includeArrayIndex: 'arrayIndex' } },
        {
          $match: match,
        },
        {
          $group: {
            _id: '$details.chartAccountDoc._id',
            amount: { $sum: '$details.amount' },
          },
        },
      ])

      let data = []

      let chartAccounts = getChartAccounts.run({
        selector: { 'accountTypeDoc.nature': { $in: ['Revenue', 'Expense'] } },
        newTotalRow: true,
      })

      chartAccounts.forEach(chart => {
        // Check total
        let parentId = ''
        if (chart.isParent) {
          parentId = chart._id
          // Is total (sub footer)
          if (chart.totalOf) {
            chart.balance = _.sumBy(data, item => {
              if (item.parent == chart.totalOf && item.totalOf) {
                return item.total
              }
            })
          }
          data.push(chart)
        } else {
          // Ge trans
          let trans = _.find(journals, { _id: chart._id })

          if (trans) {
            chart.total = trans.amount

            data.push(chart)

            // Create total
            let childTotal = _.clone(chart)
            delete childTotal.trans
            childTotal.total = chart.total
            childTotal.totalOf = childTotal._id
            childTotal.order = childTotal.order + '___'
            data.push(childTotal)
          }
        }
      })

      data = _.orderBy(data, ['order'], ['asc'])

      // Revenue
      const revenueData = _.filter(data, o => {
        return o.accountTypeDoc.nature === 'Revenue' && o.total
      })

      let totalRevenue = _.sumBy(revenueData, item => {
        if (!item.totalOf) {
          return item.total
        }
      })

      // Cost of Goods Sold
      const costOfGoodSoleData = _.filter(
        data,
        o => o.accountTypeDoc.name === 'Cost of Goods Sold'
      )

      let totalCostOfGoodSole = _.sumBy(costOfGoodSoleData, item => {
        if (!item.totalOf) {
          return item.total
        }
      })
      data = _.orderBy(data, ['order'], ['asc'])

      const grossProfit = totalRevenue - totalCostOfGoodSole

      // Expense
      const expenseData = _.filter(
        data,
        o =>
          o.accountTypeDoc.nature === 'Expense' &&
          o.accountTypeDoc.name !== 'Cost of Goods Sold' &&
          o.total
      )

      let totalExpense = _.sumBy(expenseData, item => {
        if (!item.totalOf) {
          return item.total
        }
      })

      // Net Income
      let netIncome = totalRevenue - (totalCostOfGoodSole + totalExpense)
      return {
        data: [
          { name: 'Revenue', details: revenueData, total: totalRevenue },
          {
            name: 'Cost Of Goods Sold',
            details: costOfGoodSoleData,
            total: totalCostOfGoodSole,
            grossProfit: grossProfit,
          },
          { name: 'Expense', details: expenseData, total: totalExpense },
        ],
        netIncome,
      }

      // return _.orderBy(data, ['order'], ['asc'])
    }
  },
})

rateLimit({
  methods: [reportProfitLoss],
})

export default reportProfitLoss

import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'
import moment from 'moment'

import rateLimit from '/imports/utils/rate-limit'

import { getChartAccounts } from '../lib/getChartAccounts'
import { getAccountBalance } from '../lib/getAccountBalance'
import JournalsView from '../views/Journals'

const reportGeneralLedger = new ValidatedMethod({
  name: 'app.reportGeneralLedger',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    branchId: Array,
    'branchId.$': String,
    currency: String,
    accountType: Array,
    'accountType.$': String,
    chartAccount: Array,
    'chartAccount.$': String,
    reportDate: Array,
    'reportDate.$': Date,
    rowFilter: String,
  }).validator(),
  run(params) {
    if (Meteor.isServer) {
      const dateF = moment(params.reportDate[0])
        .startOf('day')
        .toDate()
      const dateT = moment(params.reportDate[1])
        .endOf('day')
        .toDate()

      // Pick selector
      let selector = {
        tranDate: {
          $gte: dateF,
          $lte: dateT,
        },
        currency: params.currency,
      }
      if (params.branchId.length) {
        selector.branchId = { $in: params.branchId }
      }

      // Chart account filter
      let chartAccountSelector = {}
      if (params.accountType.length) {
        chartAccountSelector['details.chartAccountDoc.accountTypeId'] = {
          $in: params.accountType,
        }
      }
      if (params.chartAccount.length) {
        chartAccountSelector['details.chartAccountId'] = {
          $in: params.chartAccount,
        }
      }
      // console.log(chartAccountSelector)

      // Beginning selector
      let beginningSelector = {
        tranDate: { $lt: dateF },
        currency: params.currency,
      }
      if (params.branchId.length) {
        beginningSelector.branchId = { $in: params.branchId }
      }

      // Get journal
      let journals = JournalsView.aggregate([
        {
          $facet: {
            // beginning: [
            //   {
            //     $match: beginningSelector,
            //   },
            //   {
            //     $unwind: '$details',
            //   },
            //   {
            //     $group: {
            //       _id: '$details.chartAccountId',
            //       balance: { $sum: '$details.amount' },
            //     },
            //   },
            // ],
            trans: [
              {
                $match: selector,
              },
              {
                $sort: { tranDate: 1, refNum: 1 },
              },
              {
                $unwind: '$details',
              },
              {
                $match: chartAccountSelector,
              },
              {
                $group: {
                  _id: '$details.chartAccountId',
                  totalDr: { $sum: '$details.dr' },
                  totalCr: { $sum: '$details.cr' },
                  data: { $push: '$$ROOT' },
                },
              },
            ],
          },
        },
      ])[0]
      // console.log(journals)

      // Get account balance
      let AccountBalance = getAccountBalance.run({
        asOfDate: dateF,
        // accFilterType:,
        accFilterVales: _.clone(_.map(journals.trans, '_id')),
        // journalType,
        currency: [params.currency],
        branchId: params.branchId,
      })

      // Get chart account
      let data = []
      let chartAccountListSelector = {}
      if (params.accountType && params.accountType.length) {
        chartAccountListSelector.accountTypeId = { $in: params.accountType }
      }
      if (params.chartAccount && params.chartAccount.length) {
        chartAccountListSelector._id = { $in: params.chartAccount }
      }
      let chartAccounts = getChartAccounts.run({
        selector: chartAccountListSelector,
        newTotalRow: true,
      })
      // console.log(chartAccounts)

      chartAccounts.forEach(chart => {
        // Check total
        if (chart.isParent) {
          // parentId = chart._id
          if (chart.totalOf) {
            chart.totalDr = _.sumBy(data, item => {
              if (item.parent == chart.totalOf && item.totalOf) {
                return item.totalDr
              }
            })
            chart.totalCr = _.sumBy(data, item => {
              if (item.parent == chart.totalOf && item.totalOf) {
                return item.totalCr
              }
            })
            chart.balance = _.sumBy(data, item => {
              if (item.parent == chart.totalOf && item.totalOf) {
                return item.balance
              }
            })
          }
          data.push(chart)
        } else {
          // Get beginning
          let beginning = _.find(AccountBalance, { _id: chart._id })
          chart.balance = beginning ? beginning.balance : 0

          // Ge trans
          let totalDr = 0,
            totalCr = 0
          let trans = _.find(journals.trans, { _id: chart._id })
          let tranBalance = chart.balance
          chart.trans = []
          if (trans && trans.data) {
            trans.data.forEach(tran => {
              totalDr += tran.details.dr
              totalCr += tran.details.cr
              tran.balance = tranBalance + tran.details.amount
              tranBalance = tran.balance

              chart.trans.push(tran)
            })
          }
          data.push(chart)

          // Create total
          let childTotal = _.clone(chart)
          delete childTotal.trans
          childTotal.totalDr = totalDr
          childTotal.totalCr = totalCr
          childTotal.balance = tranBalance
          childTotal.totalOf = childTotal._id
          childTotal.order = childTotal.order + '___'
          data.push(childTotal)
        }
        // Check show rows filter
        // Code here......
      })

      // Check show rows filter
      // if (params.rowFilter === 'Active') {
      //   data = _.filter(data, o => {
      //     // if (o.isParent) {
      //     //   // o.totalCr && o.totalDr && o.balance
      //     //   if (o.totalCr && o.totalDr && o.balance)
      //     //     return _.reject(data, j => {
      //     //       return j._id == o._id
      //     //     })
      //     // } else if (
      //     //   o.totalDr ||
      //     //   o.totalCr ||
      //     //   o.balance ||
      //     //   (o.trans && o.trans.length > 0)
      //     // ) {
      //     //   return o
      //     // }
      //     return (
      //       o.isParent ||
      //       o.totalDr ||
      //       o.totalCr ||
      //       o.balance ||
      //       (o.trans && o.trans.length > 0)
      //     )
      //   })
      // }
      // else if (params.rowFilter === 'Zero') {
      //   data = _.filter(data, o => {
      //     if (
      //       o.isParent ||
      //       (o.totalDr == 0 && o.totalCr == 0 && o.balance == 0) ||
      //       (o.trans && o.trans.length == 0)
      //     ) {
      //       o.totalDr = 0
      //       o.totalCr = 0
      //       o.balance = 0

      //       return o
      //     }
      //   })
      // }
      // console.log(data)
      return _.orderBy(data, ['order'], ['asc'])
    }
  },
})

rateLimit({
  methods: [reportGeneralLedger],
})

export default reportGeneralLedger

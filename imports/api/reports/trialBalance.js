import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'
import moment from 'moment'

import rateLimit from '/imports/utils/rate-limit'

import { getChartAccounts } from '../lib/getChartAccounts'
import JournalsView from '../views/Journals'

const reportTrialBalance = new ValidatedMethod({
  name: 'app.reportTrialBalance',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    branchId: Array,
    'branchId.$': String,
    currency: String,
    showRowStatus: String,
    reportDate: Date,
  }).validator(),
  run(params) {
    if (Meteor.isServer) {
      const reportDate = moment(params.reportDate)
        .endOf('day')
        .toDate()
      const startOfYear = moment(params.reportDate)
        .startOf('year')
        .toDate()

      // Asset, Liability and Equity Selector
      let ALESelector = {
        tranDate: {
          $lte: reportDate,
        },
        currency: params.currency,
      }
      if (params.branchId.length) {
        ALESelector.branchId = { $in: params.branchId }
      }

      // Revenue and Expense Selector
      let RESelector = {
        tranDate: {
          $gte: startOfYear,
          $lte: reportDate,
        },
        currency: params.currency,
      }
      if (params.branchId.length) {
        RESelector.branchId = { $in: params.branchId }
      }

      // Get journal
      let journals = JournalsView.aggregate([
        {
          $facet: {
            ale: [
              {
                $match: ALESelector,
              },
              {
                $unwind: '$details',
              },
              {
                $group: {
                  _id: '$details.chartAccountId',
                  totalDr: { $sum: '$details.dr' },
                  totalCr: { $sum: '$details.cr' },
                  totalAmount: { $sum: '$details.amount' },
                },
              },
            ],
            re: [
              {
                $match: RESelector,
              },
              {
                $unwind: '$details',
              },
              {
                $group: {
                  _id: '$details.chartAccountId',
                  totalDr: { $sum: '$details.dr' },
                  totalCr: { $sum: '$details.cr' },
                  totalAmount: { $sum: '$details.amount' },
                },
              },
            ],
          },
        },
      ])
      journals = journals[0]

      // Get chart account
      let data = []
      let chartAccounts = getChartAccounts.run({ selector: {} })
      chartAccounts.forEach(chart => {
        // Is child
        if (!chart.isParent) {
          // Get tran
          let trans
          if (
            chart.accountTypeDoc.nature === 'Asset' ||
            chart.accountTypeDoc.nature === 'Liability' ||
            chart.accountTypeDoc.nature === 'Equity'
          ) {
            trans = _.find(journals.ale, { _id: chart._id })
          } else {
            trans = _.find(journals.re, { _id: chart._id })
          }

          chart.dr = 0
          chart.cr = 0

          if (trans) {
            // Check account type
            if (
              chart.accountTypeDoc.nature === 'Asset' ||
              chart.accountTypeDoc.nature === 'Expense'
            ) {
              trans.totalAmount >= 0
                ? (chart.dr = trans.totalAmount)
                : (chart.cr = -trans.totalAmount)
            } else {
              trans.totalAmount < 0
                ? (chart.dr = -trans.totalAmount)
                : (chart.cr = trans.totalAmount)
            }
          }

          data.push(chart)
        }
      })

      // Show row status
      if (params.showRowStatus === 'Active') {
        data = _.filter(data, o => {
          return o.dr > 0 || o.cr > 0
        })
      } else if (params.showRowStatus === 'Zero') {
        data = _.filter(data, o => {
          return o.dr == 0 && o.cr == 0
        })
      }

      // Total
      let total = {
        dr: _.sumBy(data, 'dr'),
        cr: _.sumBy(data, 'cr'),
      }

      return { data: _.sortBy(data, ['order']), total }
    }
  },
})

rateLimit({
  methods: [reportTrialBalance],
})

export default reportTrialBalance

import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'

// Lib
import rateLimit from '/imports/utils/rate-limit'

export const lookupUser = new ValidatedMethod({
  name: 'app.lookupUser',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    options: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      Meteor._sleepForMs(100)
      selector = selector || {}
      options = options || {}

      let list = []
      let data = Meteor.users.find(selector, options)
      data.forEach(o => {
        list.push({ label: o.username, value: o._id })
      })

      return list
    }
  },
})

rateLimit({
  methods: [],
})

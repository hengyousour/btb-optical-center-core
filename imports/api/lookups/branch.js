import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import { check } from 'meteor/check'

// Lib
import rateLimit from '/imports/utils/rate-limit'

// Collection
import Branches from '../branches/branches'

// Branch
export const lookupBranch = new ValidatedMethod({
  name: 'app.lookupBranch',
  mixins: [CallPromiseMixin],
  validate(selector) {
    selector = selector || {}
    check(selector, Object)
  },
  run(selector = {}) {
    if (Meteor.isServer) {
      Meteor._sleepForMs(100)
      selector = selector || {}

      let list = []
      let data = Branches.find(selector, { sort: { _id: 1 } })
      data.forEach(o => {
        list.push({ label: o.name, value: o._id })
      })

      return list
    }
  },
})

rateLimit({
  methods: [],
})

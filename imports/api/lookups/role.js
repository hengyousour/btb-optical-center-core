import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import { Roles } from 'meteor/alanning:roles'

import rateLimit from '/imports/utils/rate-limit'

import RoleGroups from '../role-groups/RoleGroups'

// Roles
export const lookupRole = new ValidatedMethod({
  name: 'app.lookupRole',
  mixins: [CallPromiseMixin],
  // validate: null,
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector }) {
    if (Meteor.isServer) {
      Meteor._sleepForMs(100)
      selector = selector || {}
      selector.name = { $nin: ['super'] }

      let data = Meteor.roles.find(selector, { sort: { name: 1 } })
      let list = data.map(o => {
        return { label: o.name, value: o._id }
      })

      return list
    }
  },
})

// Group
export const lookupRoleGroup = new ValidatedMethod({
  name: 'app.lookupRoleGroup',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector }) {
    if (Meteor.isServer) {
      Meteor._sleepForMs(100)
      selector = selector || {}

      let data = RoleGroups.find(selector, { sort: { name: 1 } })
      let list = data.map(o => {
        return { label: o.name, value: o._id, doc: o }
      })

      return list
    }
  },
})

rateLimit({
  methods: [],
})

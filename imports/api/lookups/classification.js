import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import { check } from 'meteor/check'

// Lib
import rateLimit from '/imports/utils/rate-limit'

// Collection
import Classifications from '../classifications/classifications'

// Classification
export const lookupClassification = new ValidatedMethod({
  name: 'app.lookupClassification',
  mixins: [CallPromiseMixin],
  validate(selector) {
    selector = selector || {}
    check(selector, Object)
  },
  run(selector) {
    if (Meteor.isServer) {
      Meteor._sleepForMs(100)
      selector = selector || {}
      let sort = { code: 1 }

      let list = []
      let data = Classifications.find(selector, { sort })
      data.forEach(o => {
        list.push({ label: o.name, value: o._id })
      })

      return list
    }
  },
})

rateLimit({
  methods: [],
})

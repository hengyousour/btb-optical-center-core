import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import { check } from 'meteor/check'
import _ from 'lodash'

// Lib
import rateLimit from '/imports/utils/rate-limit'

// Collection
import AccountType from '../account-types/account-types'
import ChartAccounts from '../chart-accounts/chart-accounts'

// Account Type
export const lookupAccountType = new ValidatedMethod({
  name: 'app.lookupAccountType',
  mixins: [CallPromiseMixin],
  validate(selector) {
    selector = selector || {}
    check(selector, Object)
  },
  run(selector) {
    if (Meteor.isServer) {
      Meteor._sleepForMs(100)

      selector = selector || {}
      let sort = { code: 1 }

      let list = []
      let data = AccountType.find(selector, { sort })
      data.forEach(o => {
        let custom = `
                    <span class="custom-select-opts-left">${o.name}</span>
                    <span class="custom-select-opts-right">${o.nature}</span>
                    `
        list.push({
          label: o.name,
          value: o._id,
          labelCustom: custom,
          memo: o.memo,
        })
      })

      return list
    }
  },
})

// Chart Account
export const lookupChartAccount = new ValidatedMethod({
  name: 'app.lookupChartAccount',
  mixins: [CallPromiseMixin],
  validate(selector) {
    selector = selector || {}
    check(selector, Object)
  },
  run(selector) {
    if (Meteor.isServer) {
      Meteor._sleepForMs(100)
      selector = selector || {}
      let sort = { order: 1 }

      let list = []
      let doc = ChartAccounts.aggregate([
        {
          $match: selector,
        },
        {
          $lookup: {
            from: 'app_accountTypes',
            localField: 'accountTypeId',
            foreignField: '_id',
            as: 'accountTypeDoc',
          },
        },
        {
          $unwind: '$accountTypeDoc',
        },
        {
          $sort: sort,
        },
      ])

      doc.forEach(o => {
        // Has child
        let child = _.find(doc, { parent: o._id })
        let isParent = child ? true : false

        let parent = o.accountTypeDoc.name

        // Check Parent
        let ancestorsCount = 0
        if (o.parent) {
          ancestorsCount = o.ancestors.length * 5
          parent = `-> ` + _.find(doc, { _id: o.parent }).name
        }

        let customLabel = `
                            <span class="custom-select-opts-left" style="margin-left: ${ancestorsCount}px">
                            ${o.code} : ${o.name}
                            </span>
                            <span class="custom-select-opts-right">${parent}</span>
                            `

        list.push({
          label: `${o.code} : ${o.name}`,
          value: o._id,
          labelCustom: customLabel,
          isParent,
        })
      })

      return list
    }
  },
})

rateLimit({
  methods: [],
})

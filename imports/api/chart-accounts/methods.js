import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'

import rateLimit from '/imports/utils/rate-limit'
import getNextSeq from '../../utils/get-next-seq'
import AppLog from '/imports/api/app-logs/methods'
import { throwError } from '../../utils/security'

import ChartAccounts from './chart-accounts'
import ChartAccountsView from '../views/ChartAccounts'

// Data table
export const getChartAccountsDataTable = new ValidatedMethod({
  name: 'app.getChartAccountsDataTable',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    query: {
      type: Object,
      blackbox: true,
    },
  }).validator(),
  run({ selector, query }) {
    if (Meteor.isServer) {
      selector = selector || {}

      // Options
      const options = {
        sort: { order: 1 },
        skip: (query.page - 1) * query.pageSize,
        limit: query.pageSize,
      }

      let data = ChartAccountsView.find(selector, options).fetch()
      let total = ChartAccountsView.find(selector).count()

      return { data, total }
    }
  },
})

// Find
export const findChartAccounts = new ValidatedMethod({
  name: 'app.findChartAccounts',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector }) {
    if (Meteor.isServer) {
      selector = selector || {}
      let sort = { order: 1 }

      let data = ChartAccounts.aggregate([
        {
          $match: selector,
        },
        {
          $lookup: {
            from: 'app_chartAccounts',
            localField: 'parent',
            foreignField: '_id',
            as: 'parentDoc',
          },
        },
        {
          $unwind: {
            path: '$parentDoc',
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $lookup: {
            from: 'app_accountTypes',
            localField: 'accountTypeId',
            foreignField: '_id',
            as: 'accountTypeDoc',
          },
        },
        {
          $unwind: '$accountTypeDoc',
        },
        {
          $sort: sort,
        },
      ])

      return data
    }
  },
})

// Find One
export const findOneChartAccount = new ValidatedMethod({
  name: 'app.findOneChartAccount',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    options: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}

      return ChartAccounts.findOne(selector, options)
    }
  },
})

// Insert
export const insertChartAccount = new ValidatedMethod({
  name: 'app.insertChartAccount',
  mixins: [CallPromiseMixin],
  validate: ChartAccounts.schema.validator(),
  run(doc) {
    if (Meteor.isServer) {
      const id = getNextSeq({
        // Mandatory
        _id: 'app_chartAccounts',
        // Optional
        opts: {
          seq: 1,
          // paddingType: 'start',
          // paddingLength: 6,
          // paddingChar: '0',
          // prefix: '',
          // toString: true, // default
        },
      })

      try {
        doc._id = id

        // Check sub account
        if (doc.parent) {
          let parentAccount = ChartAccounts.findOne({ _id: doc.parent })
          let ancestors = parentAccount.ancestors || []
          ancestors.push(doc.parent)

          doc.ancestors = ancestors
          doc.order = `${parentAccount.order}${doc.code}`
        } else {
          doc.order = doc.code
        }

        let result = ChartAccounts.insert(doc)

        // App log
        doc._id = result
        AppLog.create({ title: 'App.ChartOfAccount', data: doc })

        return result
      } catch (error) {
        // Decrement seq
        getNextSeq({
          _id: 'app_chartAccounts',
          opts: { seq: -1 },
        })
        throwError(error)
      }
    }
  },
})

// Update
export const updateChartAccount = new ValidatedMethod({
  name: 'app.updateChartAccount',
  mixins: [CallPromiseMixin],
  validate: _.clone(ChartAccounts.schema)
    .extend({
      _id: String,
    })
    .validator(),
  run(doc) {
    if (Meteor.isServer) {
      const _id = doc._id
      // Check sub account
      if (doc.parent) {
        let parentAccount = ChartAccountsView.findOne({
          _id: doc.parent,
        })

        let ancestors = parentAccount.ancestors || []
        ancestors.push(doc.parent)

        doc.ancestors = ancestors
        doc.order = `${parentAccount.order}${doc.code}`
      } else {
        doc.order = doc.code
      }

      let result = ChartAccounts.update({ _id }, { $set: doc })

      // App log
      doc._id = _id
      AppLog.update({ title: 'App.ChartOfAccount', data: doc })

      return result
    }
  },
})

// Remove
export const removeChartAccount = new ValidatedMethod({
  name: 'app.removeChartAccount',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: { type: String },
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      let result = ChartAccounts.remove({ _id })

      // App log
      AppLog.delete({ title: 'App.ChartOfAccount', data: { _id } })

      return result
    }
  },
})

export const removeChartAccounts = new ValidatedMethod({
  name: 'app.removeChartAccounts',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: String,
      optional: true,
    },
  }).validator(),
  run({ selector }) {
    if (Meteor.isServer) {
      selector = selector || {}
      return ChartAccounts.remove(selector)
    }
  },
})

rateLimit({
  methods: [
    getChartAccountsDataTable,
    findChartAccounts,
    findOneChartAccount,
    insertChartAccount,
    updateChartAccount,
    removeChartAccount,
  ],
})

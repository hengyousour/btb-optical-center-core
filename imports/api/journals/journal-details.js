import { Mongo } from 'meteor/mongo'
import SimpleSchema from 'simpl-schema'

const JournalDetails = new Mongo.Collection('app_journalDetails')

JournalDetails.schema = new SimpleSchema({
  journalId: {
    type: String,
  },
  chartAccountId: {
    type: String,
  },
  dr: {
    type: Number,
    min: 0,
  },
  cr: {
    type: Number,
    min: 0,
  },
  amount: {
    type: Number,
    optional: true,
  },
  memo: {
    type: String,
    optional: true,
  },
  classId: {
    type: String,
    optional: true,
  },
  // Employee, Vendor, Customer...
  nameId: {
    type: String,
    optional: true,
  },
})

JournalDetails.attachSchema(JournalDetails.schema)

export default JournalDetails

import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'

import { throwError } from '../../utils/security'
import rateLimit from '/imports/utils/rate-limit'
import getNextSeq from '../../utils/get-next-seq'
import checkJournalDetail from '../lib/checkJournalDetail'

import Journals from './journals'
import JournalDetails from './journal-details'
import JournalsView from '../views/Journals'

// Data table
export const getJournalsDataTable = new ValidatedMethod({
  name: 'app.getJournalsDataTable',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    query: {
      type: Object,
      blackbox: true,
    },
  }).validator(),
  run({ selector, query }) {
    if (Meteor.isServer) {
      selector = selector || {}

      // Options
      const options = {
        sort: { tranDate: -1 },
        skip: (query.page - 1) * query.pageSize,
        limit: query.pageSize,
      }

      let data = JournalsView.find(selector, options).fetch()
      let total = JournalsView.find(selector).count()

      return { data, total }
    }
  },
})

// Find
export const findJournals = new ValidatedMethod({
  name: 'app.findJournals',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    options: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}

      return Journals.find(selector, options).fetch()
    }
  },
})

// Find One
export const findOneJournal = new ValidatedMethod({
  name: 'app.findOneJournal',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector }) {
    if (Meteor.isServer) {
      selector = selector || {}

      let data = Journals.aggregate([
        {
          $match: selector,
        },
        {
          $lookup: {
            from: 'app_journalDetails',
            localField: '_id',
            foreignField: 'journalId',
            as: 'detailDoc',
          },
        },
      ])

      return data[0]
    }
  },
})

// Insert
export const insertJournal = new ValidatedMethod({
  name: 'app.insertJournal',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    doc: Journals.schema,
    details: {
      type: Array,
    },
    'details.$': _.clone(JournalDetails.schema).omit('journalId'),
  }).validator(),
  run({ doc, details }) {
    if (Meteor.isServer) {
      const journalId = getNextSeq({
        _id: `journal${doc.branchId}`,
        opts: {
          prefix: doc.branchId,
        },
      })

      try {
        doc._id = journalId

        // Insert Journal
        Journals.insert(doc)

        // Details
        details.forEach((item, index) => {
          // Check account type
          let { dr, cr, amount } = checkJournalDetail({
            chartAccountId: item.chartAccountId,
            dr: item.dr,
            cr: item.cr,
          })

          item._id = `${journalId}-${index}`
          item.dr = dr
          item.cr = cr
          item.amount = amount
          item.journalId = journalId

          JournalDetails.insert(item)
        })

        return journalId
      } catch (error) {
        getNextSeq({
          _id: `journal${doc.branchId}`,
          opts: { seq: -1 },
        })
        JournalDetails.remove({ journalId: journalId })
        Journals.remove({ _id: journalId })

        throwError(error)
      }
    }
  },
})

// Update
export const updateJournal = new ValidatedMethod({
  name: 'app.updateJournal',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    doc: _.clone(Journals.schema).extend({
      _id: String,
    }),
    details: {
      type: Array,
    },
    'details.$': _.clone(JournalDetails.schema).omit('journalId'),
  }).validator(),
  run({ doc, details }) {
    if (Meteor.isServer) {
      try {
        let journalId = doc._id

        // Update journal
        Journals.update({ _id: journalId }, { $set: doc })

        // Remove journal details
        JournalDetails.remove({ journalId: journalId })
        details.forEach((item, index) => {
          // Check account type
          let { dr, cr, amount } = checkJournalDetail({
            chartAccountId: item.chartAccountId,
            dr: item.dr,
            cr: item.cr,
          })

          item._id = `${journalId}-${index}`
          item.dr = dr
          item.cr = cr
          item.amount = amount
          item.journalId = journalId

          JournalDetails.insert(item)
        })

        return journalId
      } catch (error) {
        throwError(error)
      }
    }
  },
})

// Remove
export const removeJournal = new ValidatedMethod({
  name: 'app.removeJournal',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      try {
        Journals.remove({ _id })
        JournalDetails.remove({ journalId: _id })
        return _id
      } catch (error) {
        throwError(error)
      }
    }
  },
})

// Remove from External
export const removeJournalExternal = new ValidatedMethod({
  name: 'app.removeJournalExternal',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    refId: String,
    journalType: String,
  }).validator(),
  run({ refId, journalType }) {
    if (Meteor.isServer) {
      try {
        // Get previous
        const prevDoc = Journals.findOne({ refId, journalType })
        const journalId = prevDoc && prevDoc._id

        if (journalId) {
          Journals.remove({ _id: journalId })
          JournalDetails.remove({ journalId })

          return journalId
        }

        return 'null'
      } catch (error) {
        throwError(error)
      }
    }
  },
})

rateLimit({
  methods: [
    findJournals,
    findOneJournal,
    insertJournal,
    updateJournal,
    removeJournal,
  ],
})

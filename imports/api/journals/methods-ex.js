import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'
import moment from 'moment'

import { throwError } from '../../utils/security'
import rateLimit from '/imports/utils/rate-limit'
import getNextSeq from '../../utils/get-next-seq'
import { getNextRefServer } from '/imports/utils/get-next-ref.js'
import mergeDuplicateJournalDetail from '../lib/mergeDuplicateJournalDetail'
import checkJournalDetail from '../lib/checkJournalDetail'

import Journals from './journals'
import JournalDetails from './journal-details'

// Insert
const insertUpdateSchema = new SimpleSchema({
  tranDate: {
    type: Date,
  },
  journalType: {
    type: String,
  },
  currency: {
    type: String,
  },
  amount: {
    type: Number,
  },
  memo: {
    type: String,
    optional: true,
  },
  refId: {
    type: String,
    optional: true,
  },
  branchId: {
    type: String,
  },
})
export const insertJournal = new ValidatedMethod({
  name: 'app.insertJournalEx',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    doc: insertUpdateSchema,
    details: {
      type: Array,
    },
    'details.$': _.clone(JournalDetails.schema).omit('journalId'),
  }).validator(),
  run({ doc, details }) {
    if (Meteor.isServer) {
      const journalId = getNextSeq({
        _id: `journal${doc.branchId}`,
        opts: {
          prefix: doc.branchId,
        },
      })

      try {
        doc._id = journalId

        // Generate refNum
        doc.refNum = getNextRefServer({
          collectionName: 'app_journals',
          opts: {
            field: 'refNum',
            selector: {
              branchId: doc.branchId,
            },
            paddingType: 'start',
            paddingLength: 6,
            paddingChar: '0',
            prefix: moment(doc.tranDate).format('YY'),
            // prefixName: 'Vendor Deposit'
          },
        })

        // Insert Journal
        Journals.insert(doc)

        // Details
        details = mergeDuplicateJournalDetail(details)
        details.forEach((item, index) => {
          // Check account type
          let { dr, cr, amount } = checkJournalDetail({
            chartAccountId: item.chartAccountId,
            dr: item.dr,
            cr: item.cr,
          })

          item._id = `${journalId}-${index}`
          item.dr = dr
          item.cr = cr
          item.amount = amount
          item.journalId = journalId

          JournalDetails.insert(item)
        })

        return journalId
      } catch (error) {
        getNextSeq({
          _id: `journal${doc.branchId}`,
          opts: { seq: -1 },
        })
        JournalDetails.remove({ journalId: journalId })
        Journals.remove({ _id: journalId })

        throwError(error)
      }
    }
  },
})

// Update
export const updateJournal = new ValidatedMethod({
  name: 'app.updateJournalEx',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
    },
    doc: insertUpdateSchema,
    details: {
      type: Array,
    },
    'details.$': _.clone(JournalDetails.schema).omit('journalId'),
  }).validator(),
  run({ selector, doc, details }) {
    if (Meteor.isServer) {
      try {
        let journalId

        // Get previous
        const prevDoc = Journals.findOne(selector)
        if (prevDoc) {
          journalId = prevDoc._id
          // Update journal
          Journals.update({ _id: journalId }, { $set: doc })

          // Remove journal details
          JournalDetails.remove({ journalId: journalId })
          details = mergeDuplicateJournalDetail(details)
          details.forEach((item, index) => {
            // Check account type
            let { dr, cr, amount } = checkJournalDetail({
              chartAccountId: item.chartAccountId,
              dr: item.dr,
              cr: item.cr,
            })

            item._id = `${journalId}-${index}`
            item.dr = dr
            item.cr = cr
            item.amount = amount
            item.journalId = journalId

            JournalDetails.insert(item)
          })
        } else {
          journalId = insertJournal.run({ doc, details })
        }

        return journalId
      } catch (error) {
        throwError(error)
      }
    }
  },
})

// Remove
export const removeJournal = new ValidatedMethod({
  name: 'app.removeJournalEx',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    refId: String,
    journalType: String,
  }).validator(),
  run({ refId, journalType }) {
    if (Meteor.isServer) {
      try {
        // Get previous
        const prevDoc = Journals.findOne({ refId, journalType })
        const journalId = prevDoc && prevDoc._id

        if (journalId) {
          Journals.remove({ _id: journalId })
          JournalDetails.remove({ journalId })

          return journalId
        }

        return 'null'
      } catch (error) {
        throwError(error)
      }
    }
  },
})

rateLimit({
  methods: [insertJournal, updateJournal, removeJournal],
})

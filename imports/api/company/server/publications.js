import { Meteor } from 'meteor/meteor'
import Company from '../company'

Meteor.publish('app.company', () => {
  return Company.find()
})

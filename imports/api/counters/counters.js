import { Mongo } from 'meteor/mongo'
import SimpleSchema from 'simpl-schema'

const Counters = new Mongo.Collection('app_counters')

Counters.schema = new SimpleSchema({
  seq: {
    type: SimpleSchema.Integer,
  },
})

Counters.attachSchema(Counters.schema)

export default Counters

import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'

import rateLimit from '/imports/utils/rate-limit'
import getNextSeq from '../../utils/get-next-seq'
import AppLog from '/imports/api/app-logs/methods'

import AccountTypes from './account-types'
import { throwError } from '../../utils/security'

// Find
export const findAccountTypes = new ValidatedMethod({
  name: 'app.findAccountTypes',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    options: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}

      return AccountTypes.find(selector, options).fetch()
    }
  },
})

// Find One
export const findOneAccountType = new ValidatedMethod({
  name: 'app.findOneAccountType',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    options: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}

      return AccountTypes.findOne(selector, options)
    }
  },
})

// Insert
export const insertAccountType = new ValidatedMethod({
  name: 'app.insertAccountType',
  mixins: [CallPromiseMixin],
  validate: AccountTypes.schema.validator(),
  run(doc) {
    if (Meteor.isServer) {
      const id = getNextSeq({
        // Mandatory
        _id: 'app_accountTypes',
        // Optional
        opts: {
          seq: 1,
          // paddingType: 'start',
          // paddingLength: 6,
          // paddingChar: '0',
          // prefix: '',
          // toString: true, // default
        },
      })
      try {
        doc._id = id
        let result = AccountTypes.insert(doc)

        // App log
        AppLog.create({ title: 'App.AccountType', data: doc })

        return result
      } catch (error) {
        // Decrement seq
        getNextSeq({
          _id: 'app_accountTypes',
          opts: { seq: -1 },
        })
        throwError(error)
      }
    }
  },
})

// Update
export const updateAccountType = new ValidatedMethod({
  name: 'app.updateAccountType',
  mixins: [CallPromiseMixin],
  validate: _.clone(AccountTypes.schema)
    .extend({
      _id: String,
    })
    .validator(),
  run(doc) {
    if (Meteor.isServer) {
      let result = AccountTypes.update({ _id: doc._id }, { $set: doc })

      // App log
      AppLog.update({ title: 'App.AccountType', data: doc })

      return result
    }
  },
})

// Remove
export const removeAccountType = new ValidatedMethod({
  name: 'app.removeAccountType',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      let result = AccountTypes.remove({ _id })

      // App log
      AppLog.delete({ title: 'App.AccountType', data: { _id } })

      return result
    }
  },
})

export const removeAccountTypes = new ValidatedMethod({
  name: 'app.removeAccountTypes',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: String,
      optional: true,
    },
  }).validator(),
  run({ selector }) {
    if (Meteor.isServer) {
      selector = selector || {}
      return AccountTypes.remove(selector)
    }
  },
})

rateLimit({
  methods: [
    findAccountTypes,
    findOneAccountType,
    insertAccountType,
    updateAccountType,
    removeAccountType,
  ],
})

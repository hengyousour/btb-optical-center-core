import { Meteor } from 'meteor/meteor'
import SimpleSchema from 'simpl-schema'

import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'

import rateLimit from '../../utils/rate-limit'
import Employees from './employees'

// Find Employee
export const findEmployees = new ValidatedMethod({
  name: 'app.findEmployees',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    options: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}
      return Employees.find(selector, options).fetch()
    }
  },
})

// // Find One Employee
export const findOneEmployee = new ValidatedMethod({
  name: 'app.findEmployeeById',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    options: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}
      return Employees.findOne(selector, options)
    }
  },
})

// Insert Employee
export const insertEmployee = new ValidatedMethod({
  name: 'app.insertEmployee',
  mixins: [CallPromiseMixin],
  validate: Employees.schema.validator(),
  run(doc) {
    if (Meteor.isServer) {
      return Employees.insert(doc)
    }
  },
})

// Update Employee
export const updateEmployee = new ValidatedMethod({
  name: 'app.updateEmployee',
  mixins: [CallPromiseMixin],
  validate: Employees.schema.validator(),
  run(doc) {
    if (Meteor.isServer) {
      return Employees.update({ _id: doc._id }, { $set: doc })
    }
  },
})

// Remove Employee
export const removeEmployee = new ValidatedMethod({
  name: 'app.removeEmployee',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: { type: String },
  }).validator(),
  run(selector) {
    if (Meteor.isServer) {
      Employees.remove(selector)
      return 'success'
    }
  },
})

export const removeEmployees = new ValidatedMethod({
  name: 'app.removeEmployees',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      optional: true,
    },
  }).validator(),
  run({ selector }) {
    if (Meteor.isServer) {
      selector = selector || {}

      return Employees.remove(selector)
    }
  },
})

rateLimit({
  methods: [
    findEmployees,
    insertEmployee,
    removeEmployee,
    updateEmployee,
    findOneEmployee,
  ],
})

import { Mongo } from 'meteor/mongo'
import SimpleSchema from 'simpl-schema'

const Employees = new Mongo.Collection('app_employees')

// Address
const ContactSchema = new SimpleSchema({
  address: {
    type: String,
  },
  phone: {
    type: String,
    optional: true,
  },
  email: {
    type: String,
    optional: true,
  },
})

Employees.schema = new SimpleSchema({
  _id: {
    type: String,
    optional: true,
  },
  code: {
    type: String,
  },
  name: {
    type: String,
    max: 200,
  },
  gender: {
    type: String,
  },
  role: {
    type: String,
  },
  linkUser: {
    type: String,
    optional: true,
  },
  status: {
    type: String,
  },
  contact: {
    type: ContactSchema,
  },
  branchId: {
    type: String,
  },
})

Employees.attachSchema(Employees.schema)

export default Employees

import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'
import moment from 'moment'

import rateLimit from '/imports/utils/rate-limit'

import Company from '../company/company'
import Exchanges from '../exchanges/exchanges'
import JournalsView from '../views/Journals'
import { getNetIncome } from '../lib/getNetIncome'
// Get retained earnings beginning
export const getRetainedEarningsBeginning = new ValidatedMethod({
  name: 'app.getRetainedEarningsBeginning',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    asOfDate: Date,
    // Journal filter
    journalType: {
      type: Array,
      optional: true,
    },
    'journalType.$': {
      type: String,
    },
    currency: {
      type: Array,
      optional: true,
    },
    'currency.$': {
      type: String,
    },
    branchId: {
      type: Array,
      optional: true,
    },
    'branchId.$': {
      type: String,
    },
  }).validator(),
  run({ asOfDate, journalType, currency, branchId }) {
    if (Meteor.isServer) {
      // Meteor._sleepForMs(300)

      const setting = Company.findOne().setting
      const exchange = Exchanges.findOne({}, { sort: { exDate: -1 } })

      // let fiscalDate = moment(asOfDate)
      // fiscalDate.date(moment(setting.fiscalDate).format('DD'))
      // fiscalDate.month(moment(setting.fiscalDate).format('MM'))
      // fiscalDate = fiscalDate.toDate()

      // Filter
      let journalFilter = {}
      journalFilter.tranDate = { $lt: asOfDate }
      if (journalType && journalType.length) {
        journalFilter.journalType = { $in: journalType }
      }
      if (currency && currency.length) {
        journalFilter.currency = { $in: currency }
      }
      if (branchId && branchId.length) {
        journalFilter.branchId = { $in: branchId }
      }

      // Get revenue and expense
      let baseCurrencyProjection = {}
      if (setting.baseCurrency === 'USD') {
        baseCurrencyProjection = {
          $cond: {
            if: { $eq: ['$currency', 'USD'] },
            then: '$details.amount',
            else: {
              $cond: {
                if: { $eq: ['$currency', 'KHR'] },
                then: { $divide: ['$details.amount', exchange.khr] },
                else: { $divide: ['$details.amount', exchange.thb] },
              },
            },
          },
        }
      } else if (setting.baseCurrency === 'KHR') {
        baseCurrencyProjection = {
          $cond: {
            if: { $eq: ['$currency', 'KHR'] },
            then: '$details.amount',
            else: {
              $cond: {
                if: { $eq: ['$currency', 'USD'] },
                then: { $multiply: ['$details.amount', exchange.khr] },
                else: {
                  $multiply: ['$details.amount', exchange.khr / exchange.thb],
                },
              },
            },
          },
        }
      } else if (setting.baseCurrency === 'THB') {
        baseCurrencyProjection = {
          $cond: {
            if: { $eq: ['$currency', 'THB'] },
            then: '$details.amount',
            else: {
              $cond: {
                if: { $eq: ['$currency', 'USD'] },
                then: { $multiply: ['$details.amount', exchange.thb] },
                else: {
                  $multiply: ['$details.amount', exchange.thb / exchange.khr],
                },
              },
            },
          },
        }
      }

      const retainedEarningsChartAccountName = 'Retained Earnings'
      let journals = JournalsView.aggregate([
        {
          $match: journalFilter,
        },
        {
          $unwind: '$details',
        },
        {
          $match: {
            // 'details.chartAccountId': retainedEarningsChartAccountName,
            'details.accountTypeDoc.name': retainedEarningsChartAccountName,
          },
        },
        {
          $project: {
            chartAccountId: '$details.chartAccountId',
            // currency: '$currency',
            // amount: '$details.amount',
            baseCurrency: baseCurrencyProjection,
          },
        },
        {
          $group: {
            _id: '$chartAccountId',
            total: { $sum: '$baseCurrency' },
          },
        },
      ])[0]

      let retainedEarning = journals && journals.total ? journals.total : 0
      // Get net income
      // let netIncome = getNetIncome.run({
      //   asOfDate,
      //   journalType,
      //   currency,
      //   branchId,
      // })

      return retainedEarning
    }
  },
})

rateLimit({
  methods: [getRetainedEarningsBeginning],
})

import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'

import rateLimit from '/imports/utils/rate-limit'

import ChartAccountView from '../views/ChartAccounts'

export const getChartAccounts = new ValidatedMethod({
  name: 'app.getChartAccounts',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      optional: true,
    },
    newTotalRow: {
      type: Boolean,
      optional: true,
    },
  }).validator(),
  run({ selector, newTotalRow }) {
    if (Meteor.isServer) {
      Meteor._sleepForMs(300)

      // Check filter
      selector = selector || {}
      newTotalRow = newTotalRow || false
      let sort = { order: 1 }

      let getChartAccounts = ChartAccountView.find(selector, {
        sort: sort,
      }).fetch()

      let data = []
      getChartAccounts.forEach(doc => {
        // Check indent (tree level)
        doc.indent = (doc.ancestors && doc.ancestors.length) || 0

        // Check parent
        let isParent = _.find(getChartAccounts, { parent: doc._id })
        if (isParent) {
          doc.isParent = true
        }
        data.push(doc)

        // Create new total row
        if (isParent && newTotalRow) {
          let newTotalRow = _.clone(doc)
          newTotalRow.totalOf = newTotalRow._id
          newTotalRow.order = newTotalRow.order + '___'
          data.push(newTotalRow)
        }
      })

      return _.orderBy(data, ['order'], ['asc'])
    }
  },
})

export const treeChartAccountsByAllTotal = new ValidatedMethod({
  name: 'app.treeChartAccountsByTotal',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    accountType: {
      type: Array,
      optional: true,
    },
    'accountType.$': {
      type: String,
    },
    order: {
      type: Number,
      optional: true,
    },
  }).validator(),
  run({ accountType, order }) {
    if (Meteor.isServer) {
      Meteor._sleepForMs(500)

      let sort = { order: order || -1 } // Default descending

      // Check filter
      let selector = {}
      if (accountType.length) {
        selector.accountTypeId = { $in: accountType }
      }

      let getChartAccounts = ChartAccountView.find(selector, {
        sort: sort,
      }).fetch()

      // Set indent
      let data = []

      getChartAccounts.forEach(doc => {
        const count = (doc.ancestors && doc.ancestors.length) || 0
        doc.indent = _.repeat('&nbsp;', count * 5)
        data.push(doc)

        /**
         * Create total
         */
        // Check parent
        let isParent = _.find(getChartAccounts, { parent: doc._id })
        if (isParent) {
          doc.isParent = true
        }

        let cloneDoc = _.clone(doc)
        cloneDoc.totalOf = cloneDoc._id
        cloneDoc.order = cloneDoc.order + '___'
        data.push(cloneDoc)
      })

      return data
    }
  },
})

rateLimit({
  methods: [],
})

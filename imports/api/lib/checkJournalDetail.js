import SimpleSchema from 'simpl-schema'

// Collection
import ChartAccounts from '../chart-accounts/chart-accounts'

const checkJournalDetail = function({ chartAccountId, dr, cr }) {
  new SimpleSchema({
    chartAccountId: String,
    dr: {
      type: Number,
      min: 0,
    },
    cr: {
      type: Number,
      min: 0,
      custom() {
        if (
          (this.value > 0 && this.field('dr') > 0) ||
          (this.value === 0 && this.field('dr') === 0)
        ) {
          return SimpleSchema.ErrorTypes.VALUE_NOT_ALLOWED
        }
      },
    },
  }).validator({ chartAccountId, dr, cr })
  let chart = ChartAccounts.aggregate([
    {
      $match: { _id: chartAccountId },
    },
    {
      $lookup: {
        from: 'app_accountTypes',
        localField: 'accountTypeId',
        foreignField: '_id',
        as: 'accountTypeDoc',
      },
    },
    {
      $unwind: '$accountTypeDoc',
    },
  ])[0]

  if (chart && chart.accountTypeDoc.nature) {
    let amount = 0

    if (
      chart.accountTypeDoc.nature === 'Asset' ||
      chart.accountTypeDoc.nature === 'Expense'
    ) {
      if (dr > 0) {
        cr = 0
        amount = dr
      } else {
        if (cr > 0) {
          dr = 0
          amount = -cr
        } else {
          dr = 0
          cr = 0
          amount = 0
        }
      }
    }
    // 'Liability' || 'Equity' || 'Revenue':
    else if (
      chart.accountTypeDoc.nature == 'Liability' ||
      chart.accountTypeDoc.nature == 'Equity' ||
      chart.accountTypeDoc.nature == 'Revenue'
    ) {
      if (dr > 0) {
        cr = 0
        amount = -dr
      } else {
        if (cr > 0) {
          dr = 0
          amount = cr
        } else {
          dr = 0
          cr = 0
          amount = 0
        }
      }
    }

    return { doc: chart, dr, cr, amount }
  }

  return null
}

export default checkJournalDetail

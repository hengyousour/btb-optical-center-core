import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'
import moment from 'moment'

import rateLimit from '/imports/utils/rate-limit'

import { getRetainedEarningsBeginning } from './getRetainedEarningsBeginning'

import Company from '../company/company'
import JournalsView from '../views/Journals'

export const getAccountBalance = new ValidatedMethod({
  name: 'app.getAccountBalance',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    asOfDate: Date,
    // Account filter
    accFilterType: {
      type: String, // Nature, Type, Account
      optional: true,
    },
    accFilterVales: {
      type: Array,
      optional: true,
    },
    'accFilterVales.$': String,

    // Journal filter
    journalType: {
      type: Array,
      optional: true,
    },
    'journalType.$': {
      type: String,
    },
    currency: {
      type: Array,
      optional: true,
    },
    'currency.$': {
      type: String,
    },
    branchId: {
      type: Array,
      optional: true,
    },
    'branchId.$': {
      type: String,
    },
  }).validator(),
  run({
    asOfDate,
    accFilterType,
    accFilterVales,
    journalType,
    currency,
    branchId,
  }) {
    if (Meteor.isServer) {
      // Meteor._sleepForMs(300)

      const setting = Company.findOne().setting
      let fiscalDate = moment(asOfDate)
      fiscalDate.date(moment(setting.fiscalDate).format('DD'))
      fiscalDate.month(moment(setting.fiscalDate).format('MM'))
      fiscalDate = fiscalDate.toDate()

      // Filter
      let ALE_journalSelector = {}
      ALE_journalSelector.tranDate = { $lte: asOfDate }
      let RE_journalSelector = {}
      RE_journalSelector.tranDate = { $gte: fiscalDate, $lte: asOfDate }
      if (journalType && journalType.length) {
        ALE_journalSelector.journalType = { $in: journalType }
        RE_journalSelector.journalType = { $in: journalType }
      }
      if (currency && currency.length) {
        ALE_journalSelector.currency = { $in: currency }
        RE_journalSelector.currency = { $in: currency }
      }
      if (branchId && branchId.length) {
        ALE_journalSelector.branchId = { $in: branchId }
        RE_journalSelector.branchId = { $in: branchId }
      }

      let accountSelector = {}
      if (accFilterVales && accFilterVales.length) {
        if (accFilterType === 'Nature') {
          accountSelector.details.accountTypeDoc.nature = {
            $in: accFilterVales,
          }
        } else if (accFilterType === 'Type') {
          accountSelector.details.chartAccountDoc.accountTypeId = {
            $in: accFilterVales,
          }
        } else if (accFilterType === 'Account') {
          accountSelector.details.chartAccountId = { $in: accFilterVales }
        }
      }

      let journals = JournalsView.aggregate([
        {
          $facet: {
            // Asset, Liability and Equity
            AsLiEqData: [
              {
                $match: ALE_journalSelector,
              },
              {
                $unwind: '$details',
              },
              {
                $match: Object.assign(
                  {
                    'details.accountTypeDoc.nature': {
                      $in: ['Asset', 'Liability', 'Equity'],
                    },
                  },
                  accountSelector
                ),
              },
              {
                $group: {
                  _id: '$details.chartAccountId',
                  chartAccountDoc: { $last: '$details.chartAccountDoc' },
                  accountTypeDoc: { $last: '$details.accountTypeDoc' },
                  balance: { $sum: '$details.amount' },
                },
              },
            ],
            // Revenue and Expense
            ReExData: [
              {
                $match: RE_journalSelector,
              },
              {
                $unwind: '$details',
              },
              {
                $match: Object.assign(
                  {
                    'details.accountTypeDoc.nature': {
                      $in: ['Revenue', 'Expense'],
                    },
                  },
                  accountSelector
                ),
              },
              {
                $group: {
                  _id: '$details.chartAccountId',
                  chartAccountDoc: { $last: '$details.chartAccountDoc' },
                  accountTypeDoc: { $last: '$details.accountTypeDoc' },
                  balance: { $sum: '$details.amount' },
                },
              },
            ],
          },
        },
      ])[0]

      // Get net income
      let retainedEarningsBeginning = getRetainedEarningsBeginning.run({
        asOfDate,
        journalType,
        currency,
        branchId,
      })
      const retainedEarningsChartAccountName = 'Retained Earnings'
      _.find(journals.AsLiEqData, o => {
        if (o.chartAccountDoc.name === retainedEarningsChartAccountName)
          o.balance += retainedEarningsBeginning
      })

      let data = _.concat(journals.AsLiEqData, journals.ReExData)

      return _.orderBy(data, ['detail.chartAccountDoc.order'], ['asc'])
    }
  },
})

rateLimit({
  methods: [],
})

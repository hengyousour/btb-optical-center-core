import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'
import moment from 'moment'

import rateLimit from '/imports/utils/rate-limit'

import Company from '../company/company'
import Exchanges from '../exchanges/exchanges'
import JournalsView from '../views/Journals'

// Get current net income
export const getNetIncome = new ValidatedMethod({
  name: 'app.getNetIncome',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    asOfDate: Date,
    // Journal filter
    type: {
      type: String,
      allowedValues: ['Current', 'Beginning'],
    },
    journalType: {
      type: Array,
      optional: true,
    },
    'journalType.$': {
      type: String,
    },
    currency: {
      type: Array,
      optional: true,
    },
    'currency.$': {
      type: String,
    },
    branchId: {
      type: Array,
      optional: true,
    },
    'branchId.$': {
      type: String,
    },
  }).validator(),
  run({ asOfDate, type, journalType, currency, branchId }) {
    if (Meteor.isServer) {
      // Meteor._sleepForMs(300)
      type = type || 'Current'

      const setting = Company.findOne().setting
      const exchange = Exchanges.findOne({}, { sort: { exDate: -1 } })

      // Filter
      let journalFilter = {}
      journalFilter.tranDate = { $lt: asOfDate }

      if (type === 'Current') {
        let fiscalDate = moment(asOfDate)
        fiscalDate.date(moment(setting.fiscalDate).format('DD'))
        fiscalDate.month(moment(setting.fiscalDate).format('MM'))
        fiscalDate = fiscalDate.toDate()
        journalFilter.tranDate = { $gte: fiscalDate, $lte: asOfDate }
      }

      if (journalType && journalType.length) {
        journalFilter.journalType = { $in: journalType }
      }
      if (currency && currency.length) {
        journalFilter.currency = { $in: currency }
      }
      if (branchId && branchId.length) {
        journalFilter.branchId = { $in: branchId }
      }

      // Get revenue and expense
      let baseCurrencyProjection = {}
      if (setting.baseCurrency === 'USD') {
        baseCurrencyProjection = {
          $cond: {
            if: { $eq: ['$currency', 'USD'] },
            then: '$details.amount',
            else: {
              $cond: {
                if: { $eq: ['$currency', 'KHR'] },
                then: { $divide: ['$details.amount', exchange.khr] },
                else: { $divide: ['$details.amount', exchange.thb] },
              },
            },
          },
        }
      } else if (setting.baseCurrency === 'KHR') {
        baseCurrencyProjection = {
          $cond: {
            if: { $eq: ['$currency', 'KHR'] },
            then: '$details.amount',
            else: {
              $cond: {
                if: { $eq: ['$currency', 'USD'] },
                then: { $multiply: ['$details.amount', exchange.khr] },
                else: {
                  $multiply: ['$details.amount', exchange.khr / exchange.thb],
                },
              },
            },
          },
        }
      } else if (setting.baseCurrency === 'THB') {
        baseCurrencyProjection = {
          $cond: {
            if: { $eq: ['$currency', 'THB'] },
            then: '$details.amount',
            else: {
              $cond: {
                if: { $eq: ['$currency', 'USD'] },
                then: { $multiply: ['$details.amount', exchange.thb] },
                else: {
                  $multiply: ['$details.amount', exchange.thb / exchange.khr],
                },
              },
            },
          },
        }
      }

      let journals = JournalsView.aggregate([
        {
          $match: journalFilter,
        },
        {
          $unwind: '$details',
        },
        {
          $match: {
            'details.accountTypeDoc.nature': {
              $in: ['Revenue', 'Expense'],
            },
          },
        },
        {
          $project: {
            natureAccount: '$details.accountTypeDoc.nature',
            // currency: '$currency',
            // amount: '$details.amount',
            baseCurrency: baseCurrencyProjection,
          },
        },
        {
          $group: {
            _id: '$natureAccount',
            total: { $sum: '$baseCurrency' },
          },
        },
      ])

      let revenueExpense = journals.reduce((result, val) => {
        result[val._id] = val.total
        return result
      }, {})

      let netIncome =
        (revenueExpense.Revenue ? revenueExpense.Revenue : 0) -
        (revenueExpense.Expense ? revenueExpense.Expense : 0)

      // Base currency if multi currency filter
      return netIncome
    }
  },
})

rateLimit({
  methods: [],
})

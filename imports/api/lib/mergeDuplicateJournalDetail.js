import _ from 'lodash'

import JournalDetails from '../journals/journal-details'

const mergeDuplicateJournalDetail = details => {
  // let schema = _.clone(JournalDetails.schema).omit('journalId')
  // schema.validate(details)

  let data = _.reduce(
    details,
    function(results, detail) {
      let exist = exitChartAccount(results, detail.chartAccountId)

      if (exist.doc) {
        results[exist.index].dr += detail.dr
        results[exist.index].cr += detail.cr
        results[exist.index].memo += `, ${detail.memo}`
        // results[exist.index].classId += `, ${detail.classId}`
        // results[exist.index].nameId += `, ${detail.nameId}`
      } else {
        results.push(detail)
      }

      return results
    },
    []
  )

  return data
}

export default mergeDuplicateJournalDetail

/**
 * Check chart of account exist
 */
function exitChartAccount(data, id) {
  let index = _.findIndex(data, { id })
  let doc = _.find(data, { id })
  return { doc, index }
}

import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import SimpleSchema from 'simpl-schema'
import _ from 'lodash'

import { userIsInRole, throwError } from '../../utils/security'
import rateLimit from '/imports/utils/rate-limit'
import getNextSeq from '../../utils/get-next-seq'

import Branches from './branches'

// Find
export const findBranches = new ValidatedMethod({
  name: 'app.findBranches',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    selector: {
      type: Object,
      blackbox: true,
      optional: true,
    },
    options: {
      type: Object,
      blackbox: true,
      optional: true,
    },
  }).validator(),
  run({ selector, options }) {
    if (Meteor.isServer) {
      selector = selector || {}
      options = options || {}

      return Branches.find(selector, options).fetch()
    }
  },
})

// Find One
export const findOneBranch = new ValidatedMethod({
  name: 'app.findOneBranch',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      return Branches.findOne({ _id })
    }
  },
})

// Insert
export const insertBranch = new ValidatedMethod({
  name: 'app.insertBranch',
  mixins: [CallPromiseMixin],
  validate: Branches.schema.validator(),
  run(doc) {
    if (Meteor.isServer) {
      // userIsInRole(['super'])

      const id = getNextSeq({
        // Mandatory
        _id: 'app_branches',
        // Optional
        opts: {
          seq: 1,
          paddingType: 'start',
          paddingLength: 2,
          paddingChar: '0',
          // prefix: '',
          // toString: true, // default
        },
      })

      try {
        doc._id = id
        return Branches.insert(doc)
      } catch (error) {
        // Decrement seq
        getNextSeq({
          _id: 'app_branches',
          opts: { seq: -1 },
        })
        throwError(error)
      }
    }
  },
})

// Update
export const updateBranch = new ValidatedMethod({
  name: 'app.updateBranch',
  mixins: [CallPromiseMixin],
  validate: _.clone(Branches.schema)
    .extend({
      _id: String,
    })
    .validator(),
  run(doc) {
    if (Meteor.isServer) {
      // userIsInRole(['super', 'admin'])
      return Branches.update({ _id: doc._id }, { $set: doc })
    }
  },
})

// Remove
export const removeBranch = new ValidatedMethod({
  name: 'app.removeBranch',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    _id: String,
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer) {
      // userIsInRole(['super'])
      return Branches.remove({ _id })
    }
  },
})

rateLimit({
  methods: [
    findBranches,
    findOneBranch,
    insertBranch,
    updateBranch,
    removeBranch,
  ],
})

import { Meteor } from 'meteor/meteor'
import Branches from '../branches.js'

Meteor.publish('app.branches', (selector = {}, options = {}) => {
  return Branches.find(selector, options)
})

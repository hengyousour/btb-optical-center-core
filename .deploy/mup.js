module.exports = {
  servers: {
    one: {
      // TODO: set host address, username, and authentication method
      host: '178.128.23.72',
      username: 'root',
      pem: '~/.ssh/id_rsa',
      // password: 'server-password'
      // or neither for authenticate from ssh-agent
    },
  },

  app: {
    // TODO: change app name and path
    name: 'new-pos',
    path: '../',
    volumes: {
      '/data/file_uploads': '/data/file_uploads',
    },

    servers: {
      one: {},
    },

    buildOptions: {
      serverOnly: true,
    },

    env: {
      // TODO: Change to your app's url
      // If you are using ssl, it needs to start with https://
      ROOT_URL: 'http://178.128.23.72',
      MONGO_URL: 'mongodb://mongodb/new-pos',
      MONGO_OPLOG_URL: 'mongodb://mongodb/local',
      PORT: 3030,
    },

    docker: {
      // change to 'abernix/meteord:base' if your app is using Meteor 1.4 - 1.5
      image: 'abernix/meteord:node-8.4.0-base',
    },

    // The maximum number of seconds it will wait
    // for your app to successfully start (optional, default is 60)
    deployCheckWaitTime: 120,

    // Show progress bar while uploading bundle to server
    // You might need to disable it on CI servers
    enableUploadProgressBar: true,
  },

  mongo: {
    version: '3.6.4',
    servers: {
      one: {},
    },
  },

  // (Optional)
  // Use the proxy to setup ssl or to route requests to the correct
  // app when there are several apps

  // proxy: {
  //   domains: 'mywebsite.com,www.mywebsite.com',

  //   ssl: {
  //     // Enable Let's Encrypt
  //     letsEncryptEmail: 'email@domain.com'
  //   }
  // }
}
